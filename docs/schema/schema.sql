create table campaigns (
	campaign_id int unsigned not null auto_increment,
	primary key(campaign_id),
	is_base BOOLEAN not null default false,
	is_copyable BOOLEAN not null default false,
	shared_level tinyint unsigned not null,
	campaign_name varchar(50),
	user_id int unsigned not null,
	days_before_first_event tinyint unsigned not null
);
insert into campaigns values (0,1,1,2,'Real Estate Seller',1,0);



create table campaign_events (
	campaign_event_id int unsigned not null auto_increment,
	primary key(campaign_event_id),
	campaign_id int unsigned not null,

	is_event_appointment_triggered boolean not null,
	appointment_trigger_column varchar(25),
	appointment_trigger_type varchar(25),
	appointment_trigger_value varchar(25),

	next_campaign_event_id int unsigned not null,
	next_campaign_event_days tinyint unsigned not null,
	
	user_id int unsigned not null,
	event_subject varchar(80),

	is_event_to_user BOOLEAN not null,
	is_event_to_client BOOLEAN not null,
	is_starting_event BOOLEAN not null,
	
	event_body_filename varchar(255),

	event_name varchar(50)
);

insert into campaign_events values (1,1,0,'','','', 2,15,0,'Selling your home with %OFFICENAME%',0,1,1,'general_seller/day_0.eml');
insert into campaign_events values (2,1,0,'','','', 3,20,0,'You should bookmark my page',0,1,0,'general_seller/day_15.eml');
insert into campaign_events values (3,1,0,'','','', 4,30,0,'Why do some properties fail to sell?',0,1,0,'general_seller/day_36.eml');
insert into campaign_events values (4,1,0,'','','', 5,30,0,'Avoid these traps when selling!',0,1,0,'general_seller/day_70.eml');
insert into campaign_events values (4,1,0,'','','', 6,30,0,'Get a market report for your property',0,1,0,'general_seller/day_100.eml');
insert into campaign_events values (4,1,0,'','','', 7,60,0,'Properties like yours are selling',0,1,0,'general_seller/day_160.eml');
insert into campaign_events values (4,1,0,'','','', 8,30,0,'Marketing checklist for selling',0,1,0,'general_seller/day_190.eml');
insert into campaign_events values (4,1,0,'','','', 9,45,0,'Local real estate is still very active!',0,1,0,'general_seller/day_235.eml');
insert into campaign_events values (4,1,0,'','','',10,45,0,'Short selling information',0,1,0,'general_seller/day_270.eml');
insert into campaign_events values (4,1,0,'','','',11,45,0,'90 day forecast',0,1,0,'general_seller/day_315.eml');
insert into campaign_events values (4,1,0,'','','',12,45,0,'Trends are still up for selling',0,1,0,'general_seller/day_360.eml');
insert into campaign_events values (4,1,0,'','','',13,30,0,'My marketing style (Active!)',0,1,0,'general_seller/day_390.eml');
insert into campaign_events values (4,1,0,'','','',14,60,0,'Do you know anyone who needs an agent?',0,1,0,'general_seller/day_450.eml');
insert into campaign_events values (4,1,0,'','','',15,30,0,'When interviewing agents what are some important questions to ask?',0,1,0,'general_seller/day_480.eml');
insert into campaign_events values (4,1,0,'','','',16,30,0,'Let me list your home with an exit strategy',0,1,0,'general_seller/day_510.eml');
insert into campaign_events values (4,1,0,'','','',17,30,0,'Property value assessments',0,1,0,'general_seller/day_540.eml');
insert into campaign_events values (4,1,0,'','','',18,30,0,'If you have any real estate questions just ask',0,1,0,'general_seller/day_570.eml');
insert into campaign_events values (4,1,0,'','','',19,30,0,'Do you need a recommendation for a contractor or other service?',0,1,0,'general_seller/day_600.eml');
insert into campaign_events values (4,1,0,'','','',20,30,0,'Make sure your property value is right on your taxes!',0,1,0,'general_seller/day_630.eml');
insert into campaign_events values (4,1,0,'','','',21,30,0,'If your not in the market but know someone who is please send them my way',0,1,0,'general_seller/day_660.eml');
insert into campaign_events values (4,1,0,'','','',22,30,0,'What is your opinion?',0,1,0,'general_seller/day_690.eml');
insert into campaign_events values (4,1,0,'','','',23,30,0,'Let me share a story with you',0,1,0,'general_seller/day_720.eml');
0

create table clients (
	client_id int unsigned not null auto_increment,
	primary key(client_id),
	first_name varchar(25),
	last_name varchar(25),	
	date_created datetime not null
);

create table clients_rel_users (
	client_user_id int unsigned not null auto_increment,
	primary key(client_user_id),
	user_id int unsigned not null,
	client_id int unsigned not null,
	date_assigned datetime not null,
	date_deleted datetime,
	deleted_by_user_id int unsigned not null,
	assigned_by_user_id int unsigned not null,
	is_deleted BOOLEAN not null	
);

create table campaigns_rel_clients (
	campaign_client_id int unsigned not null auto_increment,
	primary key(campaign_client_id),
	campaign_id int unsigned not null,
	client_id int unsigned not null,
	date_earliest_trigger datetime,
	is_onhold boolean not null
);

create table campaign_event_rel_clients (
	campaign_event_client_id int unsigned not null auto_increment,
	primary key(campaign_event_client_id),
	campaign_event_id int unsigned not null,
	client_id int unsigned not null,
	date_to_send datetime,
	date_sent datetime
);


create table users (
	user_id int unsigned not null auto_increment,
	primary key(user_id),
	first_name varchar(25),
	last_name varchar(25),
	email varchar(75),
	password varchar(75),
	is_active BOOLEAN not null,
	company_id int unsigned not null		
);

create table companies (
	company_id int unsigned not null auto_increment,
	primary key(company_id),
	company_name varchar(25),
	is_active boolean not null
);