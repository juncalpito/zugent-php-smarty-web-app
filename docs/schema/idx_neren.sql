CREATE TABLE `agents` (
  `agent_id` varchar(25) NOT NULL,
  `date_office_assigned` datetime DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(25) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zip` varchar(5) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `date_status` datetime DEFAULT NULL,
  `license_id` varchar(255) DEFAULT NULL,
  `date_license` datetime DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `office_id` varchar(25) DEFAULT NULL,
  `agent_type` varchar(80) DEFAULT NULL,
  `phone1_desc` varchar(80) DEFAULT NULL,
  `phone1_number` varchar(15) DEFAULT NULL,
  `phone1_extension` varchar(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `idx_resource` varchar(25) DEFAULT NULL,
  `idx_class` varchar(25) DEFAULT NULL,
  `phone2_desc` varchar(25) DEFAULT NULL,
  `phone2_number` varchar(15) DEFAULT NULL,
  `phone2_extension` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `agents_raw` (
  `agent_id` varchar(25) NOT NULL,
  `json_text` text,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `listings` (
  `listing_number` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT '',
  `idx_resource` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `idx_class` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `county` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `zip` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `street_number` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `street_dir_prefix` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `street_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `unit_number` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `acres` decimal(5,5) DEFAULT NULL,
  `num_units` mediumint(8) unsigned NOT NULL,
  `list_agent_id` varchar(25) COLLATE utf8_bin NOT NULL,
  `list_office_id` varchar(25) COLLATE utf8_bin NOT NULL,
  `selling_agent_id` varchar(25) COLLATE utf8_bin NOT NULL,
  `selling_office_id` varchar(25) COLLATE utf8_bin NOT NULL,
  `date_listed` datetime DEFAULT NULL,
  `date_expired` datetime DEFAULT NULL,
  `date_price` datetime DEFAULT NULL,
  `date_status` datetime DEFAULT NULL,
  `date_contract` datetime NOT NULL,
  `date_sold` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `date_photo` datetime DEFAULT NULL,
  `date_off_market` datetime DEFAULT NULL,
  `sold_price` int(10) unsigned DEFAULT NULL,
  `photo_count` int(10) unsigned NOT NULL,
  `has_garage` tinyint(3) unsigned NOT NULL,
  `has_basement` tinyint(3) unsigned NOT NULL,
  `has_covenants` tinyint(3) unsigned NOT NULL,
  `has_easement` tinyint(3) unsigned NOT NULL,
  `has_flood_zone` tinyint(3) unsigned NOT NULL,
  `is_auction` tinyint(3) unsigned NOT NULL,
  `is_short_sale` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `condo_name` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `electric_company` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `zoning` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `village_dist_locale` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `phone_company` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `title_company` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `parcel_number` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `development` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `baths_total` tinyint(3) unsigned DEFAULT NULL,
  `beds_total` tinyint(3) unsigned DEFAULT NULL,
  `year_built` int(10) unsigned DEFAULT NULL,
  `floor_number` int(10) unsigned DEFAULT NULL,
  `finished_sqft` int(10) unsigned NOT NULL,
  `lot_sqft` int(10) unsigned NOT NULL,
  `garage_capacity` int(10) unsigned DEFAULT NULL,
  `hoa_fee` decimal(6,2) DEFAULT NULL,
  `hoa_frequency` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `school_elementry` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `school_middle` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `school_high` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `construction_status` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `has_virtual_tour` tinyint(3) unsigned NOT NULL,
  `latitude` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `longitude` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `listing_class` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `listing_type` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `total_stories` decimal(5,0) DEFAULT NULL,
  `school_district` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `owner_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `owner_phone` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `date_photo_retrieved` datetime DEFAULT NULL,
  `has_waterfront` tinyint(3) unsigned NOT NULL,
  `co_list_agent_id` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `co_list_office_id` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `baths_full` tinyint(3) unsigned DEFAULT NULL,
  `baths_half` tinyint(3) unsigned DEFAULT NULL,
  `baths_quarter` tinyint(3) unsigned DEFAULT NULL,
  `baths_three_quarter` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`listing_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `listings_raw` (
  `listing_number` varchar(15) NOT NULL DEFAULT '',
  `json_text` text,
  `mls_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`listing_number`,`mls_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `offices` (
  `office_id` varchar(25) NOT NULL DEFAULT '',
  `idx_resource` varchar(25) DEFAULT NULL,
  `idx_class` varchar(25) DEFAULT NULL,
  `main_office_id` varchar(25) DEFAULT NULL,
  `office_type` varchar(30) DEFAULT NULL,
  `office_short_name` varchar(255) DEFAULT NULL,
  `office_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zip` varchar(5) DEFAULT NULL,
  `phone1_desc` varchar(80) DEFAULT NULL,
  `phone1_number` varchar(15) DEFAULT NULL,
  `phone1_extension` varchar(7) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `webpage` varchar(80) DEFAULT NULL,
  `broker_id` varchar(25) DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `offices_raw` (
  `office_id` varchar(25) NOT NULL DEFAULT '',
  `json_text` text,
  PRIMARY KEY (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `openhouses` (
  `openhouse_id` varchar(25) NOT NULL DEFAULT '',
  `mls_id` mediumint(8) unsigned NOT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `timezone` varchar(25) DEFAULT NULL,
  `display_listing_on_internet` varchar(5) DEFAULT NULL,
  `display_address_on_internet` varchar(5) DEFAULT NULL,
  `allow_avm` varchar(5) DEFAULT NULL,
  `listing_number` varchar(25) DEFAULT NULL,
  `idx_resource` varchar(25) DEFAULT NULL,
  `idx_class` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`openhouse_id`,`mls_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `openhouses_raw` (
  `openhouse_id` varchar(25) NOT NULL DEFAULT '',
  `mls_id` mediumint(8) unsigned NOT NULL,
  `listing_number` varchar(25) DEFAULT NULL,
  `json_text` text,
  PRIMARY KEY (`openhouse_id`,`mls_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

