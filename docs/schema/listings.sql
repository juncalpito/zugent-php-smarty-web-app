CREATE TABLE `listings` (
  `MLSNo` varchar(25) NOT NULL,
  `architecture` varchar(25) DEFAULT NULL,
  `area_sqft` int(11) DEFAULT NULL,
  `bank_owned` varchar(5) DEFAULT NULL,
  `bathrooms_half` decimal(10,4) DEFAULT NULL,
  `bathrooms_full` decimal(10,4) DEFAULT NULL,
  `bathrooms_partial` tinyint(4) DEFAULT NULL,
  `bedrooms` decimal(10,4) DEFAULT NULL,
  `building_style` varchar(25) DEFAULT NULL,
  `energy_source` varchar(50) DEFAULT NULL,
  `buyers_agent_id` varchar(25) NOT NULL,
  `buyers_office_id` varchar(25) NOT NULL,
  `city` varchar(25) NOT NULL,
  `county` varchar(50) DEFAULT NULL,
  `date_canceled` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_expired` datetime DEFAULT NULL,
  `date_images_processed` datetime DEFAULT NULL,
  `date_off_market` datetime DEFAULT NULL,
  `date_sold` datetime DEFAULT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `date_temp_off_market` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `enterance_level` varchar(10) DEFAULT NULL,
  `floor_covering` varchar(80) DEFAULT NULL,
  `idx_retrieval_date` datetime DEFAULT NULL,
  `images_processed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `latitude` decimal(11,6) DEFAULT NULL,
  `listing_flow_skip` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `listing_number` varchar(25) NOT NULL,
  `listing_price` int(11) DEFAULT NULL,
  `listing_status` varchar(30) DEFAULT NULL,
  `listing_status_calculated` varchar(25) DEFAULT NULL,
  `listing_status_previous` varchar(15) NOT NULL,
  `location_bonus` varchar(10) DEFAULT NULL,
  `location_kitchen` varchar(10) DEFAULT NULL,
  `location_utility` varchar(10) DEFAULT NULL,
  `location_master` varchar(10) DEFAULT NULL,
  `longitude` decimal(11,6) DEFAULT NULL,
  `lot_acres` decimal(10,4) DEFAULT NULL,
  `lot_sqft` int(11) DEFAULT NULL,
  `mls_owner_name` varchar(50) DEFAULT NULL,
  `mls_owner_phone` varchar(50) DEFAULT NULL,
  `mls_taxes` int(11) DEFAULT NULL,
  `mls_taxes_year` mediumint(9) DEFAULT NULL,
  `monthly_rent_price` int(11) DEFAULT NULL,
  `original_listing_price` int(11) DEFAULT NULL,
  `parcel_number` varchar(50) NOT NULL,
  `parcel_number_2` varchar(50) NOT NULL,
  `phone_1` varchar(10) DEFAULT NULL,
  `phone_listed_name_1` varchar(25) DEFAULT NULL,
  `phone_type_1` varchar(25) DEFAULT NULL,
  `phone_2` varchar(10) DEFAULT NULL,
  `phone_listed_name_2` varchar(25) DEFAULT NULL,
  `phone_type_2` varchar(25) DEFAULT NULL,
  `phone_3` varchar(10) DEFAULT NULL,
  `phone_listed_name_3` varchar(25) DEFAULT NULL,
  `phone_type_3` varchar(25) DEFAULT NULL,
  `phone_4` varchar(10) DEFAULT NULL,
  `phone_listed_name_4` varchar(25) DEFAULT NULL,
  `phone_type_4` varchar(25) DEFAULT NULL,
  `phone_5` varchar(10) DEFAULT NULL,
  `phone_listed_name_5` varchar(25) DEFAULT NULL,
  `phone_type_5` varchar(25) DEFAULT NULL,
  `photo_count` int(11) DEFAULT NULL,
  `pipeline_lead_action` varchar(50) DEFAULT NULL,
  `pipeline_lead_load_date` datetime DEFAULT NULL,
  `pipeline_lead_process` datetime DEFAULT NULL,
  `pipeline_lead_prospect_id` int(11) DEFAULT NULL,
  `property_type` varchar(80) DEFAULT NULL,
  `realist_retrieval_date` datetime DEFAULT NULL,
  `realist_retrieval_status` varchar(20) DEFAULT NULL,
  `realist_tax_assessed` int(11) DEFAULT NULL,
  `realist_tax_owner_name` varchar(255) DEFAULT NULL,
  `realist_tax_year` int(11) DEFAULT NULL,
  `roof_type` varchar(25) DEFAULT NULL,
  `sellers_agent_id` varchar(25) NOT NULL,
  `sellers_office_id` varchar(25) NOT NULL,
  `sold_price` int(11) DEFAULT NULL,
  `state` varchar(25) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `street_address_calculated` varchar(255) DEFAULT NULL,
  `street_dir_prefix` varchar(32) DEFAULT NULL,
  `street_dir_suffix` varchar(32) DEFAULT NULL,
  `street_name` varchar(50) DEFAULT NULL,
  `street_number` varchar(25) NOT NULL,
  `street_number_int` int(11) NOT NULL,
  `street_suffix` varchar(32) DEFAULT NULL,
  `terms` varchar(25) DEFAULT NULL,
  `third_party_req` varchar(25) DEFAULT NULL,
  `unit_number` varchar(15) DEFAULT NULL,
  `view` varchar(25) DEFAULT NULL,
  `year_built` int(11) DEFAULT NULL,
  `zip` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`listing_id`),
  KEY `street_address` (`street_address`,`city`),
  KEY `listing_number` (`listing_number`),
  KEY `listing_status` (`listing_status`),
  KEY `date_updated` (`date_updated`),
  KEY `zip` (`zip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1






login_url = "http://neren.rets.paragonrels.com/rets/fnisrets.aspx/NEREN/login?rets-version=rets/1.7.2"
user = "gra1113"
pass = "eDKqPYyEraVQzyERCqVK"
user_agent = "x"

create table listings_raw (
	ListingNumber varchar(15),
	JSONText text
);


create table listings (
	ListingNumber varchar(15),							-- L_ListingID
	IDXResource varchar(15),
	IDXClass varchar(15),	
	Address varchar(50),
	County varchar(25),									-- L_Area
	City varchar(25),									-- L_City
	State varchar(2),									-- L_State
	Zip varchar(5),										-- L_Zip
	Price int,											-- L_AskingPrice
	ListingClass varchar(25),							-- L_Class
	ListingType varchar(25),							-- L_Type_
	StreetNumber varchar(15),							-- L_AddressNumber
	StreetDirPrefix varchar(2), 						-- L_AddressDirection
	StreetName varchar(20),								-- L_AddressStreet
	UnitNumber varchar(50),								-- L_Address2
	Status varchar(15),									-- L_StatusCatID
	Acres decimal(5,5),									-- L_NumAcres
	NumUnits mediumint unsigned not null,				-- L_NumUnits,
	ListAgentID varchar(25) not null,					-- L_ListAgent1
	ListOfficeID varchar(25) not null,					-- L_ListOffice1
	SellingAgentID varchar(25) not NULL,				-- L_SellingAgent1
	SellingOfficeID varchar(25) not null,				-- L_SellingOffice1
	DateListed datetime,								-- L_ListingDate
	DateExpired datetime,								-- L_ExpirationDate
	DatePrice datetime,									-- L_PriceDate
	DateStatus datetime									-- L_StatusDate
	DateContract datetime not null,						-- L_ContractDate
	DateSold datetime not null,							-- L_ClosingDate
	DateListed datetime,								-- L_InputDate
	DateUpdated datetime,								-- L_UpdateDate
	DatePhoto datetime,									-- L_Last_Photo_updt
	DateOffMarket datetime,								-- L_OffMarketDate
	SoldPrice int unsigned,								-- L_SoldPrice
	PhotoCount int unsigned not null,					-- L_PictureCount
	IsGarage tinyint unsigned not null,					-- LM_Char1_2
	IsBasement tinyint unsigned not null,				-- LM_Char1_6
	IsCovenants tinyint unsigned not null,				-- LM_Char1_12
	IsEasement tinyint unsigned not null,				-- LM_Char1_17
	IsFloodZone tinyint unsigned not null,				-- LM_Char1_18
	IsAuction tinyint unsigned not null,				-- LM_Char10_17
	IsShortSale varchar(25),							-- LM_Char10_24
	CondoName varchar(25),								-- LM_Char25_8
	ElectricCompany varchar(25),						-- LM_Char25_13
	Zoning varchar(25),									-- LM_Char25_17
	VillageDistLocale varchar(25),						-- LM_Char25_22
	PhoneCompany varchar(25),							-- LM_Char25_30
	TitleCompany varchar(25),							-- LM_Char50_1
	ParcelNumber varchar(25),							-- LM_Char50_3
	Development varchar(25),							-- LM_Char50_4
	BathsTotal tinyint unsigned,						-- LM_int4_36
	BedsTotal tinyint unsigned,							-- LM_Int1_6
	YearBuilt int unsigned,								-- LM_Int2_4
	FloorNumber int unsigned,							-- LM_Int2_17
	FinishedSQFT int unsigned not null,					-- LM_Int4_16
	LotSQFT int unsigned not null,						-- LM_Int4_8
	GarageCapacity int unsigned,						-- LM_Dec_8
	HOAFee decimal(6,2),								-- LM_Dec_10
	HOAFrequency varchar(25), 							-- LM_char5_5
	TotalStories decimal(5,2),							-- LM_char5_54
	SchoolDistrict varchar(25),							-- LM_char10_32
	SchoolElementry varchar(25),						-- LM_char10_68
	SchoolMiddle varchar(25),							-- LM_char10_69
	SchoolHigh varchar(50),								-- LM_Char10_2
	ConstructionStatus varchar(50),						-- LM_char50_8
	IsVirtualTour tinyint unsigned not null,			-- VT_VTourURL
	Latitude varchar(25),								-- LM_char100_5
	Longitude varchar(25)								-- LM_char100_6		
);



MLSNo = L_DisplayId
ListingID = L_ListingID
ListingType = L_Class
ListingSubType = L_Type_
Neighborhood = L_Area
Price = L_AskingPrice
PriceDate = L_PriceDate
StreetNumber = L_AddressNumber
StreetDirPrefix = L_AddressDirection
StreetName = L_AddressStreet
UnitNumber = L_Address2
City = L_City
State = L_State
Zip = L_Zip
Status = L_StatusCatID
StatusDate = L_StatusDate
SaleRent = L_SaleRent
LotAcres = L_NumAcres
TotalUnits = L_NumUnits
ListingAgent = L_ListAgent1
ListingOffice = L_ListOffice1
ListingAgent2 = L_ListAgent2
ListingOffice2 = L_ListOffice2
ListingAgent3 = L_ListAgent3
ListingOffice3 = L_ListOffice3
ListingDate = L_ListingDate
ExpirationDate = L_ExpirationDate
OriginalPrice = L_OriginalPrice
FinancialTerms = L_HowSold
ContractDate = L_ContractDate
ClosingDate = L_ClosingDate
SoldPrice = L_SoldPrice
SellingAgent = L_SellingAgent1
SellingOffice = L_SellingOffice1
HotSheetDate = L_HotSheetDate
ListingDate = L_InputDate
DateUpdated = L_UpdateDate
PhotoCount = L_PictureCount
PhotoDate = L_Last_Photo_updt
OffMarketDate = L_OffMarketDate
StreetDesignation = L_StreetDesignationId
Latitude = LMD_MP_Latitude
Longitude = LMD_MP_Longitude
GeoQuality = LMD_MP_Quality
GeoZoom = LMD_MP_ZoomLevel
GeoAddressLine = LMD_MP_AddressLine
GeoPrimaryCity = LMD_MP_PrimaryCity
GeoSecondaryCity = LMD_MP_SecondaryCity
GeoSubdivision = LMD_MP_Subdivision
GeoPostalCode = LMD_MP_PostalCode
GeoMatchCode = LMD_MP_MatchCode
GeoMatchedMethod = LMD_MP_MatchedMethod
GeoUpdateDate = LMD_MP_UpdateDate
FirstPhotoAddDate = L_FirstPhotoAddDt
ListingOffMarketDate = L_ListingsOffMarketStatusDate
LTVDate = L_LvtDate
Address = L_Address
ListingStatus = L_Status
DOM = L_DOM
DOMLS = L_DOMLS
PricePerSQFT = L_PricePerSQFT
DelayedShowing = LM_Char1_1
Garage = LM_Char1_2
MobileAnchor = LM_Char1_3
AppraisalComplete = LM_Char1_4







StreetAddress
City
State
Zip
County
Community
ListingPrice
OriginalListingPrice
Beds
Baths
HomeSqFt
$/SqFt
HOA
Status
YearBuilt
Description
PropertyType
PropoertyStyle
Views
ListingNumber
ListingAgent
ListingOffice
DateUpdated
DateCreated
DateAdded
Latitude
Longitude
PropertyTaxes
PropertyTaxYear

Interior Features
-> Bathroom Information
-> Room Information
-> Flooring Information
-> Equipment
-> Heating & Cooling


Images 
650x440
85x55
300x150


