{include file="includes/header.tpl"}

<link href="assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />

<style type="text/css">
  .timeline-buttons a {
    -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
  }
  .agent-profile img {
    border-radius: 50%;
    border: 3px solid #00acac;
    padding: 3px;
    margin: 0 auto;
    width: 120px;
    display: block;
  }
  .location-not-set {
    display: block;
    padding: 15px;
    margin: 20px 0;
    background: rgba(0, 0, 0, 0.85);
    -webkit-box-shadow: 0px 0px 15px 1px rgba(0,0,0,0.7);
    -moz-box-shadow: 0px 0px 15px 1px rgba(0,0,0,0.7);
    box-shadow: 0px 0px 15px 1px rgba(0,0,0,0.7);
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
  }
  .location-not-set h5{
    color: #fff;
  }
</style>

<!-- begin #content -->
<div id="content" class="content">

  <!-- start breadcrumb -->
  <h1 class="page-header">{$company.company_name} 
      <ol class="breadcrumb" style="display:inline-block;font-size:12px;">
          <li class="active">Drive</li>
      </ol>
  </h1>
  <!-- end breadcrumb -->

  <div id="check-in-notice" class="alert alert-success fade in m-b-15 hide">
    <strong>Check-In!</strong>
      You have successfully check-in.
    <span class="close" data-dismiss="alert">×</span>
  </div>

  <div id="browser-error" class="alert alert-danger fade in m-b-15 hide">
    <strong>Permission Denied!</strong>
      Your browser does not allow to get your location.
    <span class="close" data-dismiss="alert">×</span>
  </div>
    
  <div id="location-not-set" class="location-not-set text-center">
    <h5>Location is not set</h5>
    <a href="javascript:;" id="set-location" class="btn btn-sm btn-success">Set My Location</a>
  </div>
  
  <div id="location-set">
  <!-- <button onclick="getLocation(false)" class="btn btn-sm btn-info m-b-20">Check In Location</button> -->
  <!-- <button id="btn-drive" onclick="getLocation(true)" class="btn btn-sm btn-info m-b-20">Start Driving</button> -->

  <div id="message"></div>

  <div class="panel panel-inverse">
    <div class="panel-heading">
      <h4 class="panel-title">Map Location</h4>
    </div>
    <div class="panel-body" style="padding: 3px">
      <div style="width:100%;" id="map-container"></div>
    </div>
  </div>

  <div class="col-lg-3 col-md-12 col-sm-12">
    <div class="row">
      <div class="panel panel-inverse">
        <div class="panel-heading">
          <h4 class="panel-title">Agent Profile</h4>
        </div>
        <div class="panel-body">
          {foreach item=row key=key from=$user_locations}
            {if $key == 0}
            <div class="agent-profile text-center">
              {if $user.is_profile_photo}
              <img src="https://s3-us-west-2.amazonaws.com/zugent/profilephotos/{$row.user_id}/original.jpg" />
              {else}
              <img src="assets/img/user-13.jpg" />
              {/if}
              <i class="fa fa-user hide"></i>
              <h4>{$row.first_name} {$row.last_name}</h4>
              <p>Last Check-In: {$row.unixtime_collected|time_ago|ucfirst}</p>
              <a href="#" data-lat="{$row.latitude}" data-lon="{$row.longitude}" class="btn btn-sm btn-info m-r-5 m-b-5 btn-view-in-map"><i class="glyphicon glyphicon-eye-open"></i> View in Map</a>
              <button id="check-in" class="btn btn-sm btn-success m-r-5 m-b-5"><i class="fa fa-map-marker"></i> Check In Location</button>
              <button id="btn-drive" onclick="getLocation(true)" class="btn btn-sm btn-primary m-b-5"><i class="fa fa-car"></i> Start Driving</button>
            </div>
            {/if}
          {/foreach}
        </div>
      </div>

      <div class="panel panel-inverse" data-sortable-id="new-contacts">
        <div class="panel-heading">
          <h4 class="panel-title">Locations</h4>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table id="buckets" class="table table-striped"> 
              <thead>
                <tr>
                  <th>Last Checkin</th>
                  <th class="text-center">Location</th>
                </tr>
              </thead>
              <tbody>
              {foreach item=row from=$user_locations}
                <tr>
                  <td>{$row.unixtime_collected|time_ago|ucfirst}</td>
                  <td class="text-center">
                      <a href="javascript:;" data-lat="{$row.latitude}" data-lon="{$row.longitude}" class="btn btn-xs btn-info m-b-5 btn-view-in-map"><i class="glyphicon glyphicon-eye-open"></i>View in Map</a>
                  </td>
                  <!-- <td class="text-center">
                      <a href="javascript:;" class="btn btn-xs btn-danger m-b-5 btn-delete" data-bucket-id="{$row.user_location_id}"><i class="glyphicon glyphicon-remove"></i>Delete</a>
                  </td> -->
                </tr>
              {/foreach}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-9 col-md-12 col-sm-12">
    <div class="row">
      {if $contacts|count ne 0}
      <ul class="timeline">
        {foreach item=row from=$contacts}
        <li>
          <!-- begin timeline-time -->
          <div class="timeline-time">
            <span class="date">{$row.date_referral|date_format}</span>
            <span class="time">{$row.unixtime_referral|time_ago|ucfirst}</span>
          </div>
          <!-- end timeline-time -->
          <!-- begin timeline-icon -->
          <div class="timeline-icon">
            <a href="javascript:;"><i class="fa fa-paper-plane"></i></a>
          </div>
          <!-- end timeline-icon -->
          <!-- begin timeline-body -->
          <div class="timeline-body" style="margin-right:0px;">
            <figure>
              <img height="140" src="assets/img/client-card.jpg" />
              <!-- <div class="timeline-header">
                  <span class="username"><a href="/contacts_edit.php?id={$row.contact_id}">{$row.contact_name}</a></span>
                  <span class="pull-right text-muted">
                    <a href="javascript:;" data-lat="{$row.latitude}" data-lon="{$row.longitude}" class="btn btn-xs btn-success m-r-5 m-b-5 btn-view-in-map"><i class="glyphicon glyphicon-eye-open"></i>View in Map</a>
                    <a href="https://www.google.com/maps/dir/?api=1&destination={$row.latitude},{$row.longitude}" target="_blank" class="btn btn-xs btn-success m-r-5 m-b-5"><i class="fa fa-map-marker"></i> Directions</a>
                  </span>
              </div> -->
              <div class="timeline-content">
                <h2>{$row.first_name} <span>{$row.last_name}</span></h2>
                <p>
                  <span>Status</span> {$row.contact_status}<br>
                  <span>Phone Number</span> {$row.phone_number}<br>
                  <span>Email Address</span> {$row.email}
                </p>
              </div>
              <div class="timeline-image"><img src="assets/img/client-card.jpg"/></div>
              <div class="timeline-footer">
                <div class="icons">
                  <a href="/contacts_edit.php?id={$row.contact_id}" data-toggle="tooltip" title="Contact Page" class="orange-tooltip"><i class="fa fa-address-card"></i></a>
                  <a href="tel:{$row.phone_number}" data-toggle="tooltip" title="Call Client" class="orange-tooltip"><i class="fa fa-phone"></i></a>
                  <a href="mailto:{$row.email}" data-toggle="tooltip" title="Send Email" class="orange-tooltip"><i class="fa fa-envelope"></i></a>
                  <a href="javascript:;" data-toggle="tooltip" title="View in Map" class="orange-tooltip btn-view-in-map" data-lat="{$row.latitude}" data-lon="{$row.longitude}"><i class="fa fa-eye"></i></a>
                  <a href="https://www.google.com/maps/dir/?api=1&destination={$row.latitude},{$row.longitude}" data-toggle="tooltip" title="Directions" class="orange-tooltip" target="_blank"><i class="fa fa-map-marker"></i></a>
                </div>
                <span>Client Details</span>
              </div>
            </figure>
          </div>
          <!-- end timeline-body -->
        </li>
        {/foreach}
      </ul>
      {else}
      <div class="col-md-12">
        <div class="text-center location-not-set" style="margin:0">
          <h5>No Accepted Clients</h5>
        </div>
      </div>
      {/if}
    </div>
  </div>
  </div>

</div>
<!-- end #content -->

{include file="includes/footer.tpl" company_settings_keyval="1"}