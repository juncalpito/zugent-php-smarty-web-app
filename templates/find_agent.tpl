{include file="includes/header.tpl"}

<link href="assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />

<style type="text/css">
	#floating-panel {
		position: absolute;
		top: 70px;
	    left: 48%;
	    width: 320px;
		z-index: 20;
		background-color: #000;
		padding: 5px;
		border: 1px solid #999;
		border-radius: 6px;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
	}
	.gmap-agent-avatar img {
		border-radius: 50%;
	}
	.gmap-agent-info {
		text-align: center;
		font-size: 14px;
		font-weight: bold;
		margin-top: 5px;
	}
</style>

<!-- begin #content -->
<div id="content" class="content content-full-width">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li class="active">Find an Agent</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Find an Agent <small>All available agents</small></h1>
	<!-- end page-header -->

	<div id="message" class="hide"></div>

	<div id="browser-error" class="alert alert-danger fade in m-b-15 hide" style="margin: 35px 15px;">
		<strong>Permission Denied!</strong>
		Your browser does not allow to get your location.
		<span class="close" data-dismiss="alert">×</span>
	</div>
	
    <div id="floating-panel">
    	<form id="google-address-form" data-parsley-validate="true">
	      	<div class="input-group">
	            <input id="google-address" type="text" class="form-control" placeholder="Enter Location" data-parsley-required="true"  data-parsley-errors-container="#google-address_error">
	            <div class="input-group-btn">
	                <button type="submit" class="btn btn-success">Locate</button>
	            </div>
	        </div>
            <div id="google-address_error"></div>
        </form>
    </div>
	<div class="map">
        <div id="google-map" class="height-full width-full"></div>
    </div>
</div>
<!-- end #content -->


{include file="includes/footer.tpl"}