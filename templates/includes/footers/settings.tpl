<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
	    $("#state").val('{$company.state}');
	    $("#state").select2({ width: "100%" });
	    $("#timezone").select2({ width: "100%" });
	});
</script>