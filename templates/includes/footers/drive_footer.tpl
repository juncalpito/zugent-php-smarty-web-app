<script src="assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="assets/plugins/sweetalert2/js/sweetalert2.all.min.js"></script>
<script src="assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAq8WuTeXonjSzYw_vVLiN7PCRQNqf_v_Q"></script>

<script type="text/javascript">
  var x = document.getElementById("message");
  var driving = false;

  function savePosition(lat, lon) {
      $.ajax({
          type: "POST",
          url: '/ws/user/set_location',
          data: { 'lat': lat, 'lon': lon },
          dataType: "json",
          success: function(result) {
              console.log(result);
              location.reload();
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
              console.log('XHR ERROR ' + XMLHttpRequest.status + ' ' + textStatus + ' ' + errorThrown);
          }
      });
  }

  function getLocation(isDriving) {
      if (navigator.geolocation) {
        if(isDriving) 
          navigator.geolocation.getCurrentPosition(drive, showError);
        else
          navigator.geolocation.getCurrentPosition(showPosition, showError);
      } else { 
          x.innerHTML = "Geolocation is not supported by this browser.";
      }
  }

  function drive(position) {

      if (driving == false) {
        $("#btn-drive").html("<i class='fa fa-ban'></i> Stop Driving");
        $("#btn-drive").removeClass("btn-primary");
        $("#btn-drive").addClass("btn-danger");
        driving = true;
      }
      else {
        $("#btn-drive").html("<i class='fa fa-car'></i> Start Driving");
        $("#btn-drive").removeClass("btn-danger");
        $("#btn-drive").addClass("btn-primary");
        driving = false;
      }

      var driveInterval;

      if (driving) {

        lat = position.coords.latitude;
        lon = position.coords.longitude;
        latlon = new google.maps.LatLng(lat, lon);
        mapholder = document.getElementById('map-container');
        mapholder.style.height = '300px';
        mapholder.style.width = '100%';

        var myOptions = {
        center:latlon,zoom:16,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
        mapTypeControl:false,
        navigationControlOptions:{ style:google.maps.NavigationControlStyle.SMALL }
        }
        
        var map = new google.maps.Map(document.getElementById("map-container"), myOptions);
        var marker = new google.maps.Marker({ position:latlon,map:map,title:"You are here!" });
      

      driveInterval = setInterval(function () {

        $.ajax({
          type: "POST",
          url: '/ws/user/set_location',
          data: { 
            'lat': lat, 
            'lon': lon,
            'driving': 'true',
          },
          dataType: "json",
          success: function(response) {
            if (driving) {
            console.log(response);
            latlon = new google.maps.LatLng(lat, lon);
            marker.setPosition(latlon);
            }
            else {
              clearInterval(driveInterval);
            }
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log('XHR ERROR ' + XMLHttpRequest.status + ' ' + textStatus + ' ' +errorThrown);
          }
        });

      }, 5000);

      }
      else
        clearInterval(driveInterval);

  }

  function showPosition(position) {
      lat = position.coords.latitude;
      lon = position.coords.longitude;
      latlon = new google.maps.LatLng(lat, lon);
      mapholder = document.getElementById('map-container');
      mapholder.style.height = '300px';
      mapholder.style.width = '100%';

      var myOptions = {
      center:latlon,zoom:16,
      mapTypeId:google.maps.MapTypeId.ROADMAP,
      mapTypeControl:false,
      navigationControlOptions:{ style:google.maps.NavigationControlStyle.SMALL }
      }
      
      var map = new google.maps.Map(document.getElementById("map-container"), myOptions);
      var marker = new google.maps.Marker({ position:latlon,map:map,title:"You are here!" });

      savePosition(lat,lon);
  }

  function viewPosition(latlon) {
      mapholder = document.getElementById('map-container')
      mapholder.style.height = '300px';
      mapholder.style.width = '100%';

      var myOptions = {
      center:latlon,zoom:16,
      mapTypeId:google.maps.MapTypeId.ROADMAP,
      mapTypeControl:false,
      navigationControlOptions:{ style:google.maps.NavigationControlStyle.SMALL }
      }
      
      var map = new google.maps.Map(document.getElementById("map-container"), myOptions);
      var marker = new google.maps.Marker({ position:latlon,map:map,title:"You are here!" });

  }

  function showError(error) {
      switch(error.code) {
          case error.PERMISSION_DENIED:
              x.innerHTML = "User denied the request for Geolocation."
              break;
          case error.POSITION_UNAVAILABLE:
              x.innerHTML = "Location information is unavailable."
              break;
          case error.TIMEOUT:
              x.innerHTML = "The request to get user location timed out."
              break;
          case error.UNKNOWN_ERROR:
              x.innerHTML = "An unknown error occurred."
              break;
      }
  }

  function initMap(latitude,longitude) {
    var pos = new google.maps.LatLng(latitude, longitude);
    viewPosition(pos);
  }

  function checkPermission(result, position=null) {
    if (result) {
      swal({
        title: 'Location Set!',
        type: 'success',
        allowOutsideClick: false
      }).then(function () {
        $('#browser-error').addClass('hide');
        showPosition(position);
      }, function (dismiss) {});
    }
    else {
      $('#browser-error').removeClass('hide');
    }
  }

  // Geolocation API
  // https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBuYrziPHytz3v0V6pzPDrcAFNe0wxS-GA

  $(document).ready(function() {

    {if $user_locations|count eq 0}
      $('#location-not-set').css('display','block');
      $('#location-set').css('display','none');
    {else}
      $('#location-not-set').css('display','none');
      $('#location-set').css('display','block');
      initMap({$user_latitude},{$user_longitude});
    {/if}

    $(document).on("click", ".btn-view-in-map", function() { 
      if (navigator.geolocation) {
        console.log($(this).data("lat") + " , " + $(this).data("lon"))
        var pos = new google.maps.LatLng($(this).data("lat"), $(this).data("lon"));
        viewPosition(pos);
      } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
      }
    });

    $(document).on("click", "#set-location", function() { 

      navigator.geolocation.watchPosition(function(position) {
        checkPermission(true, position);
      },
      function (error) { 
        if (error.code == error.PERMISSION_DENIED)
          checkPermission(false);
      });

    });

    $(document).on("click", "#check-in", function() {

      swal({
        title: 'You have checked in!',
        html: 'Page will reload to apply changes',
        type: 'success',
        allowOutsideClick: false
      }).then(function () {
        getLocation(false);
      }, function (dismiss) {});

    });

  });
</script>

 <script>
      // var map, infoWindow;
      // function initMap() {
      //   map = new google.maps.Map(document.getElementById('map-container'), {
      //     center: { lat: -34.397, lng: 150.644 },
      //     zoom: 17
      //   });
      //   infoWindow = new google.maps.InfoWindow;

      //   var userLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      //   console.log(userLatLng);

      //   infoWindow.setPosition(pos);
      //   infoWindow.setContent('Location found.');
      //   infoWindow.open(map);
      //   map.setCenter(userLatLng);
      //   // Try HTML5 geolocation.
      //   // if (navigator.geolocation) {
      //   //   navigator.geolocation.getCurrentPosition(function(position) {
      //   //     var pos = {
      //   //       lat: position.coords.latitude,
      //   //       lng: position.coords.longitude
      //   //     };
      //   //   }, function() {
      //   //     handleLocationError(true, infoWindow, map.getCenter());
      //   //   });
      //   // } else {
      //   //   // Browser doesn't support Geolocation
      //   //   handleLocationError(false, infoWindow, map.getCenter());
      //   // }
      // }

      // function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      //   infoWindow.setPosition(pos);
      //   infoWindow.setContent(browserHasGeolocation ?
      //                         'Error: The Geolocation service failed.' :
      //                         'Error: Your browser doesn\'t support geolocation.');
      //   infoWindow.open(map);
      // }
</script>