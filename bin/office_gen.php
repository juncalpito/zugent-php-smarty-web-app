<?PHP

require_once($_SERVER['SITE_DIR']."/includes/common.php");

$shortopts = "";
$longopts = array(
	'company_id::',
	'name::',
	'req_name::',
	'mls_db::'
);
$options = getopt($shortopts, $longopts);

if (!isset($options['req_name'])) {
	print "--req_name required\n";
	exit;
} elseif (!isset($options['company_id'])) {
	print "--company_id required\n";
	exit;
} elseif (!isset($options['mls_db'])) {
	print "--mls_db required\n";
	exit;
}

$sql = "select * from zugent.mls where mls_database='".SQL_CLEAN($options['mls_db'])."'";
print $sql."\n";
$sth = SQL_QUERY($sql);

if (SQL_NUM_ROWS($sth) == 0) {
	print "Could not locate that MLS\n";
	exit;
}
$mls_data = SQL_ASSOC_ARRAY($sth);


$sql = "select office_id,office_name,city,state,phone1_number, zip from ".$options['mls_db'].".offices where office_name like '%".SQL_CLEAN($options['req_name'])."%'";
if (isset($options['name'])) {
	if (count($options['name']) > 1) {
		$sql .= " and (";
		$fe_first = 1;
		foreach ($options['name'] as $oname) {
			if (!$fe_first) { $sql .= " or "; } else { $fe_first = 0; }
			$sql .= " office_name like '%".SQL_CLEAN($oname)."%'";
		}
		$sql .= ")";
	} else {
		$sql .= " and office_name like '%".$options['name']."%'";
	}
}
print $sql."\n";
$sth = SQL_QUERY($sql);
while ($data = SQL_ASSOC_ARRAY($sth)) {

	$sth_o = SQL_QUERY("select * from zugent.offices as o left join zugent.office_rel_offices as oro on o.office_id=oro.office_id where o.company_id='".$options['company_id']."' and oro.mls_office_id='".$data['office_id']."'");
	if (SQL_NUM_ROWS($sth_o) == 0) {
		print "ADDING: ".$data['office_id'].": ".$data['office_name']."\n";
		$sth_a = SQL_QUERY("
			insert into zugent.offices (
				company_id
				, name
				, city
				, state
				, zip
				, phone_office
				, is_active
			) values (
				'".$options['company_id']."'
				,'".SQL_CLEAN($data['office_name'])."'
				,'".SQL_CLEAN($data['city'])."'
				,'".SQL_CLEAN($data['state'])."'
				,'".SQL_CLEAN($data['zip'])."'
				,'".SQL_CLEAN($data['phone1_number'])."'
				,1
			)
		");
		$office_id = SQL_INSERT_ID();
		SQL_QUERY("insert into zugent.office_rel_offices (office_id, mls_id, mls_office_id) values ('".$office_id."','".$mls_data['mls_id']."','".$data['office_id']."')");
	} else {
		print "SKIPPING: ".$data['office_id'].": ".$data['office_name']."\n";
	}
}
print "\n";


#	where office_name like '%century 21%' and (office_name like '%northshore%' or office_name like '%north shore%' or office_name like '%NSGroup%' or office_name like '%ns group%')
#");
#while ($data = SQL_ASSOC_ARRAY($sth)) {
#}
