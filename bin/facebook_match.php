<?php
require_once($_SERVER['SITE_DIR']."/includes/common.php");

define (FULLCONTACT_KEY, 'KXVcUF0qd2Z40LIkhJkGCUcHsj0cCrhd');
define (FULLCONTACT_DAILY_LIMIT, 120);

$sth = SQL_QUERY("select email from zugent.fullcontact where (facebook_url != '' or linkedin_url != '' or twitter_url != '') and is_success=1 and date_added > date_sub(now(), interval 24 hour)");
$contacts_24h = SQL_NUM_ROWS($sth);

if ($contacts_24h >= FULLCONTACT_DAILY_LIMIT) {
	print "Daily limit of ".FULLCONTACT_DAILY_LIMIT." reached. ($contacts_24h)\n";
	exit;
}

while ($contacts_24h < FULLCONTACT_DAILY_LIMIT) {
	$sth = SQL_QUERY("select e.email,c.contact_id from zugent.contacts as c left join zugent.contact_emails as e on e.contact_id=c.contact_id and e.is_primary=1 left join zugent.fullcontact as fc on fc.email=e.email where fc.fullcontact_id is null and e.email is not null and e.email like '%@%' and e.email != '' and e.email not like '%;%' limit 1");
	if (SQL_NUM_ROWS($sth) == 0) {
		print "All data has been requested\n";
		exit;
	}
	$data = SQL_ASSOC_ARRAY($sth);
	
	print $contacts_24h." / ".FULLCONTACT_DAILY_LIMIT.": ";
	print $data['email'].": ";
	$result = fullcontact($data['email']);
	$fc_array = json_decode($result, TRUE);
	if ($fc_array['code'] > 0) {
		print "CODE FAILURE: ".$fc_array['code']."\n";
	} elseif ($fc_array['status'] > 0) {
		print "STATUS FAILURE: ".$fc_array['status']."\n";
	} else {
		print "SUCCESS: ".$fc_array['facebook']." ".$fc_array['avatar']."\n";
	}

	$sth = SQL_QUERY("select email from zugent.fullcontact where (facebook_url != '' or linkedin_url != '' or twitter_url != '') and is_success=1 and date_added > date_sub(now(), interval 24 hour)");
	$contacts_24h = SQL_NUM_ROWS($sth);
}

function fullcontact($email, $force = 0) {
	sleep(1);
	$data_string = '{"email": "'.$email.'"}';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.fullcontact.com/v3/person.enrich/"); # URL to post to
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); # return into a variable
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.FULLCONTACT_KEY)); # custom headers, see above
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	
	$result = curl_exec( $ch ); # run!
	curl_close($ch);

	$fc_array = json_decode($result, TRUE);

	if ($fc_array['status'] == '429') {
		print "ERROR: USAGE LIMITS HAVE EXCEEDED!\n";
		exit;
	}

	if ($fc_array['code']) {
		$sth = SQL_QUERY("insert into zugent.fullcontact (email, json_data, is_success, retcode, date_added) values ('".SQL_CLEAN($email)."', '".SQL_CLEAN($result)."', 0, '".SQL_CLEAN($fc_array['code'])."', NOW())");
	} elseif ($fc_array['status']) {
		$sth = SQL_QUERY("insert into zugent.fullcontact (email, json_data, is_success, retcode, date_added) values ('".SQL_CLEAN($email)."', '".SQL_CLEAN($result)."', 0, '".SQL_CLEAN($fc_array['status'])."', NOW())");
	} else {
		$sth = SQL_QUERY("
			insert into zugent.fullcontact (
				email
				, json_data
				, is_success
				, date_added
				, facebook_url
				, facebook_photo_url
				, agerange
				, gender
				, location
				, age
				, bio
				, linkedin_url
				, twitter_url
			) values (
				'".SQL_CLEAN($email)."'
				, '".SQL_CLEAN($result)."'
				, 1
				, NOW()
				, '".SQL_CLEAN($fc_array['facebook'])."'
				, '".SQL_CLEAN($fc_array['avatar'])."'
				, '".SQL_CLEAN($fc_array['ageRange'])."'
				, '".SQL_CLEAN($fc_array['gender'])."'
				, '".SQL_CLEAN($fc_array['location'])."'
				, '".SQL_CLEAN($fc_array['details']['age']['value'])."'
				, '".SQL_CLEAN($fc_array['bio'])."'
				, '".SQL_CLEAN($fc_array['linkedin'])."'
				, '".SQL_CLEAN($fc_array['twitter'])."'
			)
		");
	}
	return $result;
}



?>