#!/bin/bash


php idx_listings.php --mls_id=1 --start_relative="$1" --step_size="$2" --resource="Property" --class="RE_1" --skip_photos=1
php idx_listings.php --mls_id=1 --start_relative="$1" --step_size="$2" --resource="Property" --class="LD_2" --skip_photos=1
php idx_listings.php --mls_id=1 --start_relative="$1" --step_size="$2" --resource="Property" --class="CS_3" --skip_photos=1
php idx_listings.php --mls_id=1 --start_relative="$1" --step_size="$2" --resource="Property" --class="CL_4" --skip_photos=1
php idx_listings.php --mls_id=1 --start_relative="$1" --step_size="$2" --resource="Property" --class="MF_5" --skip_photos=1
php idx_listings.php --mls_id=1 --start_relative="$1" --step_size="$2" --resource="Property" --class="BF_6" --skip_photos=1
php idx_listings.php --mls_id=1 --start_relative="$1" --step_size="$2" --resource="Property" --class="RN_7" --skip_photos=1

#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=a*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=b*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=c*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=d*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=e*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=f*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=g*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=h*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=i*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=j*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=k*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=l*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=m*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=n*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=o*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=p*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=q*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=r*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=s*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=t*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=u*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=v*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=w*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=x*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=y*,"
#php idx_agents.php --start_relative="5 year" --step_size="5 year" --class="ActiveAgent" --resource="ActiveAgent" --query="U_UserFirstName=z*,"

