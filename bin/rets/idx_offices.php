<?PHP
global $mls_ini, $office_ini;

require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$shortopts = "";
$longopts = array(
	'start_relative::',
	'start_absolute::',
	'step_size::',
	'offset::',
	'debug::',
	'query::',
	'mls::',
	'config::'
);
$options = getopt($shortopts, $longopts);

$mls_ini = parse_ini_file($_SERVER['SITE_DIR']."/etc/mls/".$options['mls']."/mls.ini", true);
$office_ini = parse_ini_file($_SERVER['SITE_DIR']."/etc/mls/".$options['mls']."/".$options['config'].".ini", true);

$config = new \PHRETS\Configuration;
$config->setLoginUrl($mls_ini['MLS']['login_url']);
$config->setUsername($mls_ini['MLS']['user']);
$config->setPassword($mls_ini['MLS']['pass']);
$config->setRetsVersion($mls_ini['MLS']['rets_version']);
$config->setUserAgent($mls_ini['MLS']['user_agent']);
$config->setHttpAuthenticationMethod($mls_ini['MLS']['authentication_method']);

$rets = new \PHRETS\Session($config);
$rets->Login();

if (isset($options['start_relative'])) {
	$sth = SQL_QUERY("select unix_timestamp(date_sub(now(),interval ".$options['start_relative']."))");
	list ($starting) = SQL_ROW($sth);
} elseif (isset($options['start_absolute'])) {
	$sth = SQL_QUERY("select unix_timestamp('".$options['start_absolute']."')");
	list ($starting) = SQL_ROW($sth);
}

if (isset($options['step_size'])) {
	$sth = SQL_QUERY("select unix_timestamp(date_add(now(),interval ".$options['step_size'].")) - unix_timestamp(now())");
	list ($step_size) = SQL_ROW($sth);
} else {
	$sth = SQL_QUERY("select unix_timestamp(date_add(now(),interval 6 hour)) - unix_timestamp(now())");
	list ($step_size) = SQL_ROW($sth);
}

print getLogTS().": -------------------------------------------------------------\n";
print getLogTS().": ZugEnt RETS Offices\n";
print getLogTS().": -------------------------------------------------------------\n";
print getLogTS().": Step Size:     ".$step_size."\n";
print getLogTS().": Starting Date: ".$starting."\n";
print getLogTS().": Current Time:  ".time()."\n";



if (isset($office_ini['Definition']['skip_options']) && $office_ini['Definition']['skip_options']) {
	$record_count = pullRecords("NILL", "NILL", 0);
	if ($mls_ini['MLS']['offset'] > 0 && $record_count >= $mls_ini['MLS']['offset']) {
		while ($record_count >= $mls_ini['MLS']['offset']) {
			$offset += $mls_ini['MLS']['offset'];
			$record_count = pullRecords("NILL", "NILL", $offset);
		}
	}
} else {
	$step_pass = 0;
	while ($starting < time()) {
		$step_pass++;
		$sth_curtime = SQL_QUERY("select unix_timestamp()");
		list($end_time) = SQL_ROW($sth_curtime);
		$begindate = gmdate('Y-m-d\TH:i:s', $starting);
		$enddate = gmdate('Y-m-d\TH:i:s', $starting + $step_size);
		$offset = 0;
		$record_count = pullRecords($begindate, $enddate, 0);
		if ($mls_ini['MLS']['offset'] > 0 && $record_count >= $mls_ini['MLS']['offset']) {
			while ($record_count >= $mls_ini['MLS']['offset']) {
				$offset += $mls_ini['MLS']['offset'];
				$record_count = pullRecords($begindate, $enddate, $offset);
			}
		}
		$starting += $step_size;
	}
}

$rets->Disconnect();

function pullRecords($begindate, $enddate, $offset) {
	global $rets, $options, $config, $mls_ini, $options, $office_ini;

	$query = "(".$options['query'].$office_ini['Columns']['date_updated']."=".$begindate."-".$enddate.")";
	if (isset($office_ini['Definition']['skip_options']) && $office_ini['Definition']['skip_options']) $query = "";

	if (isset($mls_ini['MLS']['offset']) && $mls_ini['MLS']['offset'] > 0) {
		$search = $rets->Search($office_ini['Definition']['resource'], $office_ini['Definition']['class'], $query, array('Offset' => $offset, 'Limit' => $mls_ini['MLS']['offset'], 'Format' => 'COMPACT-DECODED'));
	} else {
		$search = $rets->Search($office_ini['Definition']['resource'], $office_ini['Definition']['class'], $query, array('Format' => 'COMPACT-DECODED'));
	}

	print getLogTS().":".$office_ini['Definition']['resource']."/".$office_ini['Definition']['class']." [".$search->getReturnedResultsCount()."/".$offset."] [".$begindate." - ".$enddate."]: ";
	if ($search->getReturnedResultsCount() > 0) {
		foreach ($search as $record) {
			processOffice($record);
		}
		print "\n";
	} else {
		print "No data for this time period.\n";
	}
	return $search->getTotalResultsCount();
}

function processOffice($record) {
	global $rets, $config, $options, $mls_ini, $options, $office_ini;

	if ($mls_ini['MLS']['mls_id'] == 2) $record = cleanMLSPIN($record);

	$columns['office_id'] = $record[$office_ini['Columns']['office_id']];
	$columns['idx_resource'] = $office_ini['Definition']['resource'];
	$columns['idx_class'] = $office_ini['Definition']['class'];
	if (isset($office_ini['Columns']['office_id'])) $columns['office_id'] = $record[$office_ini['Columns']['office_id']];
	if (isset($office_ini['Columns']['main_office_id'])) $columns['main_office_id'] = $record[$office_ini['Columns']['main_office_id']];
	if (isset($office_ini['Columns']['office_type'])) $columns['office_type'] = $record[$office_ini['Columns']['office_type']];
	if (isset($office_ini['Columns']['office_short_name'])) $columns['office_short_name'] = $record[$office_ini['Columns']['office_short_name']];
	if (isset($office_ini['Columns']['office_name'])) $columns['office_name'] = $record[$office_ini['Columns']['office_name']];
	if (isset($office_ini['Columns']['address'])) $columns['address'] = $record[$office_ini['Columns']['address']];
	if (isset($office_ini['Columns']['address2'])) $columns['address2'] = $record[$office_ini['Columns']['address2']];
	if (isset($office_ini['Columns']['city'])) $columns['city'] = $record[$office_ini['Columns']['city']];
	if (isset($office_ini['Columns']['state'])) $columns['state'] = $record[$office_ini['Columns']['state']];
	if (isset($office_ini['Columns']['zip'])) $columns['zip'] = $record[$office_ini['Columns']['zip']];
	if (isset($office_ini['Columns']['phone1_desc'])) $columns['phone1_desc'] = $record[$office_ini['Columns']['phone1_desc']];
	if (isset($office_ini['Columns']['phone1_number'])) $columns['phone1_number'] = $record[$office_ini['Columns']['phone1_number']];
	if (isset($office_ini['Columns']['phone1_extension'])) $columns['phone1_extension'] = $record[$office_ini['Columns']['phone1_extension']];
	if (isset($office_ini['Columns']['email'])) $columns['email'] = $record[$office_ini['Columns']['email']];
	if (isset($office_ini['Columns']['webpage'])) $columns['webpage'] = $record[$office_ini['Columns']['webpage']];
	if (isset($office_ini['Columns']['broker_id'])) $columns['broker_id'] = $record[$office_ini['Columns']['broker_id']];
	if (isset($office_ini['Columns']['date_updated'])) $columns['date_updated'] = date_cleanup($record[$office_ini['Columns']['date_updated']]);
	if (isset($office_ini['Columns']['date_added'])) $columns['date_added'] = date_cleanup($record[$office_ini['Columns']['date_added']]);

	$sth = SQL_QUERY("SELECT office_id from ".$mls_ini['MLS']['database'].".offices where office_id='".$record[$office_ini['Columns']['office_id']]."' limit 1");
	if (SQL_NUM_ROWS($sth) == 0) {

		$sql = "INSERT INTO ".$mls_ini['MLS']['database'].".offices (";
		$sql .= "`".implode("`,`", array_keys($columns))."`";
		$sql .=") values (";
		$sql .= "'".implode("', '", array_map('SQL_CLEAN', $columns))."'";
		$sql .= ")";
		SQL_QUERY($sql);

		SQL_QUERY("INSERT INTO ".$mls_ini['MLS']['database'].".offices_raw (office_id, json_text) values ('".SQL_CLEAN($record[$office_ini['Columns']['office_id']])."', '".SQL_CLEAN($record->toJson())."')");
		print "I";
	} else {
		$sql = "UPDATE ".$mls_ini['MLS']['database'].".offices SET ";
		$first = 0;
		foreach ($columns as $k => $v) {
			if ($k == 'office_id') continue(1);
			if (!$first) {
				$sql .= $k."='".SQL_CLEAN($v)."'";
				$first = 1;
			} else {
				$sql .= ",".$k."='".SQL_CLEAN($v)."'";
			}
		}
		$sql .= " where office_id='".SQL_CLEAN($record[$office_ini['Columns']['office_id']])."'";
		SQL_QUERY($sql);

		SQL_QUERY("UPDATE ".$mls_ini['MLS']['database'].".offices_raw SET json_text='".SQL_CLEAN($record->toJson())."' WHERE office_id='".SQL_CLEAN($record[$office_ini['Columns']['office_id']])."' LIMIT 1");
		print "U";
	}

}



function cleanMLSPIN($record) {
	global $rets, $config, $options, $mls_ini, $options, $office_ini;

	$record[$office_ini['Columns']['phone1_number']] = $record['Office_Area_Code'].$record['Office_Phone'];

	return $record;
}


function date_cleanup($string) {
	$string = str_replace("T"," ", $string);
	return $string;
}

/* Get Current Time */
function getLogTS() {
	return date('Y-m-d H:i:s');
}
	
?>
