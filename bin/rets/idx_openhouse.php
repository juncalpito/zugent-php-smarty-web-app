<?PHP
global $config_ini, $config;

require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$config_ini = parse_ini_file($_SERVER['SITE_DIR']."/etc/neren_resi.ini",true);

$config = new \PHRETS\Configuration;
$config->setLoginUrl("http://neren.rets.paragonrels.com/rets/fnisrets.aspx/NEREN/login?rets-version=rets/1.7.2")
		->setUsername("gra1113")
		->setPassword("eDKqPYyEraVQzyERCqVK")
		->setRetsVersion('1.7.2');

$rets = new \PHRETS\Session($config);
$rets->Login();

$shortopts = "";
$longopts = array(
	'start_relative::',
	'start_absolute::',
	'step_size::',
	'offset::',
	'debug::',
	'resource::',
	'class::',
	'query::'
);
$options = getopt($shortopts, $longopts);

if (isset($options['start_relative'])) {
	$sth = SQL_QUERY("select unix_timestamp(date_sub(now(),interval ".$options['start_relative']."))");
	list ($starting) = SQL_ROW($sth);
} elseif (isset($options['start_absolute'])) {
	$sth = SQL_QUERY("select unix_timestamp('".$options['start_absolute']."')");
	list ($starting) = SQL_ROW($sth);
} else {
	print "DEFINE RELATIVE OR ABSOLUTE STARTING POSITION\n";
	exit;
}

if (isset($options['step_size'])) {
	$sth = SQL_QUERY("select unix_timestamp(date_add(now(),interval ".$options['step_size'].")) - unix_timestamp(now())");
	list ($step_size) = SQL_ROW($sth);
} else {
	$sth = SQL_QUERY("select unix_timestamp(date_add(now(),interval 6 hour)) - unix_timestamp(now())");
	list ($step_size) = SQL_ROW($sth);
}

print "Step Size: ".$step_size."\n";
print "Starting Date: ".$starting."\n";

$sth_curtime = SQL_QUERY("select unix_timestamp()");
list($end_time) = SQL_ROW($sth_curtime);

$step_pass = 0;
while ($starting < $end_time) {
	$step_pass++;
	$sth_curtime = SQL_QUERY("select unix_timestamp()");
	list($end_time) = SQL_ROW($sth_curtime);
	$begindate = gmdate('Y-m-d\TH:i:s', $starting);
	$enddate = gmdate('Y-m-d\TH:i:s', $starting + $step_size);
	$offset = 0;
	$record_count = pullRecords($begindate, $enddate, 0);
	if ($config_ini['offset'] > 0 && $record_count >= $config_ini['offset']) {
		while ($record_count >= $mls_config['offset_limit']) {
			$offset += $config_ini['offset'];
			$record_count = pullRecords($begindate, $enddate, $offset);
		}
	}
	$starting += $step_size;
}
	
$rets->Disconnect();

function pullRecords($begindate, $enddate, $offset) {
	global $rets, $options, $config, $config_ini, $options, $mls_config, $log_id, $log_added, $log_updated, $log_to_process, $log_warnings;

	// $query = "(".$options['query'].$config_ini['OpenHouseColumns']['DateUpdated']."=".$begindate."-".$enddate.")";
	$query = "(".$options['query'].$config_ini['OpenHouseColumns']['DateUpdated']."=".$begindate."+)";
	print "Query: ".$query."\n";
	if ($config_ini['offset'] > 0) {
		$search = $rets->Search($options['resource'], $options['class'], $query, array('Offset' => $offset, 'Format' => 'COMPACT-DECODED'));
	} else {
		$search = $rets->Search($options['resource'], $options['class'], $query, array('Format' => 'COMPACT-DECODED'));
	}

	print $search->getReturnedResultsCount()." records returned.\n";

//	if ($search->getReturnedResultsCount() > 0) {
		print getLogTS().":".$options['resource']."/".$options['class']." [".$search->getReturnedResultsCount()."/".$offset."] [".$begindate." - ".$enddate."]: ";
		foreach ($search as $record) {
			processOpenHouse($record);
		}
		print "\n";
//	}
	return $search->getTotalResultsCount();
}

function processOpenHouse($record) {
	global $rets, $config, $options, $config_ini, $options;

//	print_r($record->toArray());
//	exit;

	$sth = SQL_QUERY("SELECT OpenHouseID from ".$config_ini['database'].".openhouses where OpenHouseID='".$record[$config_ini['OpenHouseColumns']['OpenHouseID']]."' limit 1");
	if (SQL_NUM_ROWS($sth) == 0) {
		$sth = SQL_QUERY("
			INSERT INTO ".$config_ini['database'].".openhouses (
				OpenHouseID,
				IDXResource,
				IDXClass,
				DateStart,
				DateEnd,
				Comments,
				DateAdded,
				DateUpdated,
				Timezone,
				DisplayListingOnInternet,
				DisplayAddressOnInternet,
				AllowAVM,
				ListingNumber
			) VALUES (
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['OpenHouseID']])."',
				'".SQL_CLEAN($options['resource'])."',
				'".SQL_CLEAN($options['class'])."',
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DateStart']])."',
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DateEnd']])."',
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['Comments']])."',
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DateAdded']])."',
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DateUpdated']])."',
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['Timezone']])."',
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DisplayListingOnInternet']])."',
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DisplayAddressOnInternet']])."',
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['AllowAVM']])."',
				'".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['ListingNumber']])."'
			)
		");
		SQL_QUERY("INSERT INTO ".$config_ini['database'].".openhouses_raw (OpenHouseID,ListingNumber,JSONText) VALUES ('".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['OpenHouseID']])."','".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['ListingNumber']])."','".SQL_CLEAN($record->toJson())."')");
		print "I";
	} else {
		$sth = SQL_QUERY("
			UPDATE ".$config_ini['database'].".openhouses SET
				OpenHouseID='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['OpenHouseID']])."',
				IDXResource='".SQL_CLEAN($options['resource'])."',
				IDXClass='".SQL_CLEAN($options['class'])."',
				DateStart='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DateStart']])."',
				DateEnd='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DateEnd']])."',
				Comments='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['Comments']])."',
				DateAdded='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DateAdded']])."',
				DateUpdated='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DateUpdated']])."',
				Timezone='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['Timezone']])."',
				DisplayListingOnInternet='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DisplayListingOnInternet']])."',
				DisplayAddressOnInternet='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['DisplayAddressOnInternet']])."',
				AllowAVM='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['AllowAVM']])."',
				ListingNumber='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['ListingNumber']])."'
			WHERE
				OpenHouseID='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['OpenHouseID']])."'
		");
		SQL_QUERY("UPDATE ".$config_ini['database'].".offices_raw SET JSONText='".SQL_CLEAN($record->toJson())."' WHERE OpenHouseID='".SQL_CLEAN($record[$config_ini['OpenHouseColumns']['OpenHouseID']])."' LIMIT 1");
		print "U";
	}
}

/* Get Current Time */
function getLogTS() {
	return date('Y-m-d H:i:s');
}
	
?>