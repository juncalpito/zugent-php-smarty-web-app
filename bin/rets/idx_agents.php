<?PHP
global $mls_ini, $agent_ini;

require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$shortopts = "";
$longopts = array(
	'start_relative::',
	'start_absolute::',
	'step_size::',
	'offset::',
	'debug::',
	'query::',
	'mls::',
	'config::'
);
$options = getopt($shortopts, $longopts);

$mls_ini = parse_ini_file($_SERVER['SITE_DIR']."/etc/mls/".$options['mls']."/mls.ini", true);
$agent_ini = parse_ini_file($_SERVER['SITE_DIR']."/etc/mls/".$options['mls']."/".$options['config'].".ini", true);

$config = new \PHRETS\Configuration;
$config->setLoginUrl($mls_ini['MLS']['login_url']);
$config->setUsername($mls_ini['MLS']['user']);
$config->setPassword($mls_ini['MLS']['pass']);
$config->setRetsVersion($mls_ini['MLS']['rets_version']);
$config->setUserAgent($mls_ini['MLS']['user_agent']);
$config->setHttpAuthenticationMethod($mls_ini['MLS']['authentication_method']);

$rets = new \PHRETS\Session($config);
$rets->Login();

if (isset($options['start_relative'])) {
	$sth = SQL_QUERY("select unix_timestamp(date_sub(now(),interval ".$options['start_relative']."))");
	list ($starting) = SQL_ROW($sth);
} elseif (isset($options['start_absolute'])) {
	$sth = SQL_QUERY("select unix_timestamp('".$options['start_absolute']."')");
	list ($starting) = SQL_ROW($sth);
}

if (isset($options['step_size'])) {
	$sth = SQL_QUERY("select unix_timestamp(date_add(now(),interval ".$options['step_size'].")) - unix_timestamp(now())");
	list ($step_size) = SQL_ROW($sth);
} else {
	$sth = SQL_QUERY("select unix_timestamp(date_add(now(),interval 6 hour)) - unix_timestamp(now())");
	list ($step_size) = SQL_ROW($sth);
}

print getLogTS().": -------------------------------------------------------------\n";
print getLogTS().": ZugEnt RETS Agents\n";
print getLogTS().": -------------------------------------------------------------\n";
print getLogTS().": Step Size:     ".$step_size."\n";
print getLogTS().": Starting Date: ".$starting."\n";
print getLogTS().": Current Time:  ".time()."\n";

if (isset($agent_ini['Definition']['skip_options']) && $agent_ini['Definition']['skip_options']) {

	$record_count = pullRecords("NILL", "NILL", 0);
	if ($mls_ini['MLS']['offset'] > 0 && $record_count >= $mls_ini['MLS']['offset']) {
		while ($record_count >= $mls_ini['MLS']['offset']) {
			$offset += $mls_ini['MLS']['offset'];
			$record_count = pullRecords("NILL", "NILL", $offset);
		}
	}

} else {
	$step_pass = 0;
	while ($starting < time()) {
		$step_pass++;
		$sth_curtime = SQL_QUERY("select unix_timestamp()");
		list($end_time) = SQL_ROW($sth_curtime);
		$begindate = gmdate('Y-m-d\TH:i:s', $starting);
		$enddate = gmdate('Y-m-d\TH:i:s', $starting + $step_size);
		$offset = 0;
		$record_count = pullRecords($begindate, $enddate, 0);
		if ($mls_ini['MLS']['offset'] > 0 && $record_count >= $mls_ini['MLS']['offset']) {
			while ($record_count >= $mls_ini['MLS']['offset']) {
				$offset += $mls_ini['MLS']['offset'];
				$record_count = pullRecords($begindate, $enddate, $offset);
			}
		}
		$starting += $step_size;
	}
}

$rets->Disconnect();

function pullRecords($begindate, $enddate, $offset) {
	global $rets, $options, $config, $mls_ini, $options, $agent_ini;

	$query = "(".$options['query'].$agent_ini['Columns']['date_updated']."=".$begindate."-".$enddate.")";
	if (isset($agent_ini['Definition']['skip_options']) && $agent_ini['Definition']['skip_options']) $query = "";

	if (isset($mls_ini['MLS']['offset']) && $mls_ini['MLS']['offset'] > 0) {
		$search = $rets->Search($agent_ini['Definition']['resource'], $agent_ini['Definition']['class'], $query, array('Offset' => $offset, 'Limit' => $mls_ini['MLS']['offset'], 'Format' => 'COMPACT-DECODED'));
	} else {
		$search = $rets->Search($agent_ini['Definition']['resource'], $agent_ini['Definition']['class'], $query, array('Format' => 'COMPACT-DECODED'));
	}

	print getLogTS().":".$agent_ini['Definition']['resource']."/".$agent_ini['Definition']['class']." [".$search->getReturnedResultsCount()."/".$offset."] [".$begindate." - ".$enddate."]: ";
	if ($search->getReturnedResultsCount() > 0) {
		foreach ($search as $record) {
			processAgent($record);
		}
		print "\n";
	} else {
		print "No data for this time period.\n";
		return 0;
	}
	return $search->getTotalResultsCount();
}

function cleanMLSPIN($record) {
	global $rets, $config, $options, $mls_ini, $options, $agent_ini;

	$record[$agent_ini['Columns']['phone1_number']] = $record[$agent_ini['Columns']['phone1_area']].$record[$agent_ini['Columns']['phone1_number']];

	return $record;
}

function processAgent($record) {
	global $rets, $config, $options, $mls_ini, $options, $agent_ini;

	if ($mls_ini['MLS']['mls_id'] == 2) $record = cleanMLSPIN($record);

	$columns['agent_id'] = $record[$agent_ini['Columns']['agent_id']];
	$columns['idx_resource'] = $agent_ini['Definition']['resource'];
	$columns['idx_class'] = $agent_ini['Definition']['class'];
	if (isset($agent_ini['Columns']['office_id'])) $columns['office_id'] = $record[$agent_ini['Columns']['office_id']];
	if (isset($agent_ini['Columns']['date_office_assigned'])) $columns['date_office_assigned'] = $record[$agent_ini['Columns']['date_office_assigned']];
	if (isset($agent_ini['Columns']['first_name'])) $columns['first_name'] = $record[$agent_ini['Columns']['first_name']];
	if (isset($agent_ini['Columns']['last_name'])) $columns['last_name'] = $record[$agent_ini['Columns']['last_name']];
	if (isset($agent_ini['Columns']['address'])) $columns['address'] = $record[$agent_ini['Columns']['address']];
	if (isset($agent_ini['Columns']['address2'])) $columns['address2'] = $record[$agent_ini['Columns']['address2']];
	if (isset($agent_ini['Columns']['city'])) $columns['city'] = $record[$agent_ini['Columns']['city']];
	if (isset($agent_ini['Columns']['state'])) $columns['state'] = $record[$agent_ini['Columns']['state']];
	if (isset($agent_ini['Columns']['zip'])) $columns['zip'] = $record[$agent_ini['Columns']['zip']];
	if (isset($agent_ini['Columns']['status'])) $columns['status'] = $record[$agent_ini['Columns']['status']];
	if (isset($agent_ini['Columns']['date_status'])) $columns['date_status'] = $record[$agent_ini['Columns']['date_status']];
	if (isset($agent_ini['Columns']['license_id'])) $columns['license_id'] = $record[$agent_ini['Columns']['license_id']];
	if (isset($agent_ini['Columns']['date_license'])) date_cleanup($columns['date_license'] = $record[$agent_ini['Columns']['date_license']]);
	if (isset($agent_ini['Columns']['birth_date'])) date_cleanup($columns['birth_date'] = $record[$agent_ini['Columns']['birth_date']]);
	if (isset($agent_ini['Columns']['date_updated'])) date_cleanup($columns['date_updated'] = $record[$agent_ini['Columns']['date_updated']]);
	if (isset($agent_ini['Columns']['date_added'])) date_cleanup($columns['date_added'] = $record[$agent_ini['Columns']['date_added']]);
	if (isset($agent_ini['Columns']['agent_type'])) $columns['agent_type'] = $record[$agent_ini['Columns']['agent_type']];
	if (isset($agent_ini['Columns']['phone1_desc'])) $columns['phone1_desc'] = $record[$agent_ini['Columns']['phone1_desc']];
	if (isset($agent_ini['Columns']['phone1_number'])) $columns['phone1_number'] = $record[$agent_ini['Columns']['phone1_number']];
	if (isset($agent_ini['Columns']['phone1_extension'])) $columns['phone1_extension'] = $record[$agent_ini['Columns']['phone1_extension']];
	if (isset($agent_ini['Columns']['email'])) $columns['email'] = $record[$agent_ini['Columns']['email']];
	if (isset($agent_ini['Columns']['phone2_desc'])) $columns['phone2_desc'] = $record[$agent_ini['Columns']['phone2_desc']];
	if (isset($agent_ini['Columns']['phone2_number'])) $columns['phone2_number'] = $record[$agent_ini['Columns']['phone2_number']];
	if (isset($agent_ini['Columns']['phone2_extension'])) $columns['phone2_extension'] = $record[$agent_ini['Columns']['phone2_extension']];


	$sth = SQL_QUERY("SELECT agent_id from ".$mls_ini['MLS']['database'].".agents where agent_id='".$record[$agent_ini['Columns']['agent_id']]."' limit 1");
	if (SQL_NUM_ROWS($sth) == 0) {
		$sql = "INSERT INTO ".$mls_ini['MLS']['database'].".agents (";
		$sql .= "`".implode("`,`", array_keys($columns))."`";
		$sql .=") values (";
		$sql .= "'".implode("', '", array_map('SQL_CLEAN', $columns))."'";
		$sql .= ")";
		SQL_QUERY($sql);
		SQL_QUERY("INSERT INTO ".$mls_ini['MLS']['database'].".agents_raw (agent_id, json_text) values ('".SQL_CLEAN($record[$agent_ini['Columns']['agent_id']])."', '".SQL_CLEAN($record->toJson())."')");
		print "I";
	} else {
		$sql = "UPDATE ".$mls_ini['MLS']['database'].".agents SET ";
		$first = 0;
		foreach ($columns as $k => $v) {
			if ($k == 'agent_id') continue(1);
			if (!$first) {
				$sql .= $k."='".SQL_CLEAN($v)."'";
				$first = 1;
			} else {
				$sql .= ",".$k."='".SQL_CLEAN($v)."'";
			}
		}
		$sql .= " where agent_id='".SQL_CLEAN($record[$agent_ini['Columns']['agent_id']])."'";
		SQL_QUERY($sql);
		SQL_QUERY("UPDATE ".$mls_ini['MLS']['database'].".agents_raw set json_text = '".SQL_CLEAN($record->toJson())."' where agent_id = '".SQL_CLEAN($record[$agent_ini['Columns']['agent_id']])."'");
		print "U";
	}

}

function date_cleanup($string) {
	$string = str_replace("T"," ", $string);
	return $string;
}

/* Get Current Time */
function getLogTS() {
	return date('Y-m-d H:i:s');
}

?>
