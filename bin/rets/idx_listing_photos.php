<?PHP
global $config_ini, $config, $mls_ini, $options;

require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$shortopts = "";
$longopts = array(
	'debug::',
	'ln::',
	'mls::',
	'config::',
	'limit::'
);
$options = getopt($shortopts, $longopts);

$mls_ini = parse_ini_file($_SERVER['SITE_DIR']."/etc/mls/".$options['mls']."/mls.ini", true);
$config_ini = parse_ini_file($_SERVER['SITE_DIR']."/etc/mls/".$options['mls']."/".$options['config'].".ini", true);


use Aws\S3\S3Client;
$s3Client = S3Client::factory(array(
    'key'    => "AKIAJ7MLE7RT54HBO6XQ",
    'secret' => "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"
));


print getLogTS().":#-------------------------------------------------------------\n";
print getLogTS().":# ZugEnt RETS Photos\n";
print getLogTS().":#-------------------------------------------------------------\n";
print getLogTS().":# MLS Config: ".$options['mls']."\n";
print getLogTS().":# Config: ".$options['config']."\n";

if ($options['limit'] > 0) {
	$sth = SQL_QUERY("select listing_number,date_photo, date_photo_retrieved,photo_count from ".$mls_ini['MLS']['database'].".listings where date_updated > date_sub(now(),interval 1 year) and photo_count > 0 and (date_photo_retrieved < date_photo or date_photo_retrieved is null) order by date_updated DESC LIMIT ".$options['limit']);
} else {
	$sth = SQL_QUERY("select listing_number,date_photo, date_photo_retrieved,photo_count from ".$mls_ini['MLS']['database'].".listings where date_updated > date_sub(now(),interval 1 year) and photo_count > 0 and (date_photo_retrieved < date_photo or date_photo_retrieved is null) order by date_updated DESC");
}




print getLogTS().": ".SQL_NUM_ROWS($sth)." listings found\n";

if (SQL_NUM_ROWS($sth) == 0) exit;

$config = new \PHRETS\Configuration;
$config->setLoginUrl($mls_ini['MLS']['login_url'])
	->setUsername($mls_ini['MLS']['user'])
	->setPassword($mls_ini['MLS']['pass'])
	->setRetsVersion($mls_ini['MLS']['rets_version']);

$rets = new \PHRETS\Session($config);
$rets->Login();


while ($listing = SQL_ASSOC_ARRAY($sth)) {

	print getLogTS().":".$config_ini['Photos']['resource']."/".$config_ini['Photos']['class']." [".$listing['listing_number']." : ".$listing['photo_count']."] ";
	downloadPhotos($listing['listing_number']);
	SQL_QUERY("update ".$mls_ini['MLS']['database'].".listings set date_photo_retrieved='".SQL_CLEAN($listing['date_photo'])."' where listing_number='".$listing['listing_number']."' limit 1");
	print "\n";
}

/* Get Current Time */
function getLogTS() {
	return date('Y-m-d H:i:s');
}

function downloadPhotos($listing_id) {
	global $rets, $config, $options, $config_ini, $s3Client, $mls_ini;

	if ($options['debug']) {
		print "\n";
		print "Listing ID: ".$listing_id."\n";
		print "Resource: ".$config_ini['Photos']['resource']."\n";
		print "Resource: ".$config_ini['Photos']['class']."\n";
	}
	$objects = $rets->GetObject($config_ini['Photos']['resource'], $config_ini['Photos']['class'], $listing_id);
	$photo_numb = 0;
	foreach ($objects as $object) {
		if ($object->IsError()) {
			print_r($object->getError());
			continue(1);
		}

		$photo_numb++;
		$filename = $mls_ini['MLS']['s3_prefix'].$listing_id."_".$photo_numb.".jpg";

		file_put_contents($filename, $object->getContent());
        if (file_exists($filename) && filesize($filename) > 100) {
            $result = $s3Client->putObject(array(
				'Bucket'     => 'zugidx-raw',
				'Key'        => $filename,
				'SourceFile' => $filename
            ));
			print "-";
			generateThumb($listing_id, 650, $filename);
			generateThumb($listing_id, 85, $filename);
			generateThumb($listing_id, 300, $filename);
        }
        @unlink($filename);
	}
}

function generateThumb($listing_id, $thumbSize, $filename) {
	global $rets, $config, $options, $config_ini, $s3Client, $log_id, $log_added, $log_updated, $mls_config;

    list($width, $height) = getimagesize($filename);
    $myImage = imagecreatefromjpeg($filename);
    if ($width > $height) {
        $y = 0;
        $x = ($width - $height) / 2;
        $smallestSide = $height;
    } else {
        $x = 0;
        $y = ($height - $width) / 2;
        $smallestSide = $width;
    }
    $thumb = imagecreatetruecolor($thumbSize, $thumbSize);
    imagecopyresampled($thumb, $myImage, 0, 0, $x, $y, $thumbSize, $thumbSize, $smallestSide, $smallestSide);
    imagejpeg($thumb, 'temp_'.$filename, 80);
    $result = $s3Client->putObject(array(
            'Bucket'     => 'zugidx',
            'Key'        => $thumbSize.'/'.$filename,
            'SourceFile' => 'temp_'.$filename
    ));
    @unlink('temp_'.$filename);
	print ".";
}

?>
