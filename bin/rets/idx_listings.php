<?PHP
global $mls_ini, $config_ini;

require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$shortopts = "";
$longopts = array(
	'start_relative::',
	'start_absolute::',
	'step_size::',
	'offset::',
	'debug::',
	'query::',
	'mls::',
	'config::'
);
$options = getopt($shortopts, $longopts);

$mls_ini = parse_ini_file($_SERVER['SITE_DIR']."/etc/mls/".$options['mls']."/mls.ini", true);
$config_ini = parse_ini_file($_SERVER['SITE_DIR']."/etc/mls/".$options['mls']."/".$options['config'].".ini", true);

$config = new \PHRETS\Configuration;
$config->setLoginUrl($mls_ini['MLS']['login_url']);
$config->setUsername($mls_ini['MLS']['user']);
$config->setPassword($mls_ini['MLS']['pass']);
$config->setRetsVersion($mls_ini['MLS']['rets_version']);
$config->setUserAgent($mls_ini['MLS']['user_agent']);
$config->setHttpAuthenticationMethod($mls_ini['MLS']['authentication_method']);

$rets = new \PHRETS\Session($config);
$rets->Login();

if (isset($options['start_relative'])) {
	$sth = SQL_QUERY("select unix_timestamp(date_sub(now(),interval ".$options['start_relative']."))");
	list ($starting) = SQL_ROW($sth);
} elseif (isset($options['start_absolute'])) {
	$sth = SQL_QUERY("select unix_timestamp('".$options['start_absolute']."')");
	list ($starting) = SQL_ROW($sth);
} else {
	print "DEFINE RELATIVE OR ABSOLUTE STARTING POSITION\n";
	exit;
}

if (isset($options['step_size'])) {
	$sth = SQL_QUERY("select unix_timestamp(date_add(now(),interval ".$options['step_size'].")) - unix_timestamp(now())");
	list ($step_size) = SQL_ROW($sth);
} else {
	$sth = SQL_QUERY("select unix_timestamp(date_add(now(),interval 6 hour)) - unix_timestamp(now())");
	list ($step_size) = SQL_ROW($sth);
}

print getLogTS().":#-------------------------------------------------------------\n";
print getLogTS().":# ZugEnt RETS Listings\n";
print getLogTS().":#-------------------------------------------------------------\n";
print getLogTS().":# Step Size:     ".$step_size."\n";
print getLogTS().":# Starting Date: ".$starting."\n";
print getLogTS().":# Current Time:  ".time()."\n";

$step_pass = 0;
while ($starting < time()) {
	$step_pass++;
	$sth_curtime = SQL_QUERY("select unix_timestamp()");
	list($end_time) = SQL_ROW($sth_curtime);
	$begindate = gmdate('Y-m-d\TH:i:s', $starting);
	$enddate = gmdate('Y-m-d\TH:i:s', $starting + $step_size);
	$offset = 0;
	$record_count = pullRecords($begindate, $enddate, 0);
	if ($mls_ini['MLS']['offset'] > 0 && $record_count >= $mls_ini['MLS']['offset']) {
		while ($record_count >= $mls_ini['MLS']['offset']) {
			$offset += $mls_ini['MLS']['offset'];
			$record_count = pullRecords($begindate, $enddate, $offset);
		}
	}
	$starting += $step_size;
}

$rets->Disconnect();

function pullRecords($begindate, $enddate, $offset) {
	global $rets, $options, $config, $mls_ini, $options, $config_ini;

	$query = "(".$options['query'].$config_ini['Columns']['date_updated']."=".$begindate."-".$enddate.")";

	if ($mls_ini['MLS']['offset'] > 0) {
		$search = $rets->Search($config_ini['Definition']['resource'], $config_ini['Definition']['class'], $query, array('Offset' => $offset, 'Format' => 'COMPACT-DECODED'));
	} else {
		$search = $rets->Search($config_ini['Definition']['resource'], $config_ini['Definition']['class'], $query, array('Format' => 'COMPACT-DECODED'));
	}

	print getLogTS().":".$config_ini['Definition']['resource']."/".$config_ini['Definition']['class']." [".$search->getReturnedResultsCount()."/".$offset."] [".$begindate." - ".$enddate."]: ";
	if ($search->getReturnedResultsCount() > 0) {
		foreach ($search as $record) {
			processListings($record);
		}
		print "\n";
	} else {
		print "No data for this time period.\n";
		return 0;
	}
	return $search->getTotalResultsCount();
}

function date_cleanup($string) {
	$string = str_replace("T"," ", $string);
	return $string;
}

function processListings($record) {
	global $rets, $config, $options, $mls_ini, $options, $config_ini;

	if ($mls_ini['MLS']['mls_id'] == 1) {
		// $record = cleanupNEREN($record);
	}

	$address = address_generator($record);

	if ($mls_ini['MLS']['mls_id'] == 1) {
		$has_waterfront = ($record[$config_ini['Columns']['has_waterfront']] != "No" ? 1 : 0);
		$has_garage = ($record[$config_ini['Columns']['has_garage']] != "No" ? 1 : 0);
		$has_basement = ($record[$config_ini['Columns']['has_basement']] != "No" ? 1 : 0);
		$is_auction = ($record[$config_ini['Columns']['is_auction']] != "No" ? 1: 0);
		$is_short_sale = ($record[$config_ini['Columns']['is_short_sale']] != "No" ? 1 : 0);
		$has_virtual_tour = ($record[$config_ini['Columns']['has_virtual_tour']] != "" ? 1 : 0);
		if ($record[$config_ini['Columns']['has_covenants']] == "Unknown" || $record[$config_ini['Columns']['has_covenants']] == "No") {
			$has_covenants = 0;
		} else {
			$has_covenants = 1;
		}

		if ($record[$config_ini['Columns']['has_easement']] == "Unknown" || $record[$config_ini['Columns']['has_easement']] == "No") {
			$has_easement = 0;
		} else {
			$has_easement = 1;
		}

		if ($record[$config_ini['Columns']['has_flood_zone']] == "Unknown" || $record[$config_ini['Columns']['has_flood_zone']] == "No") {
			$has_flood_zone = 0;
		} else {
			$has_flood_zone = 1;
		}
	} elseif ($mls_ini['MLS']['mls_id'] == 2) {
		$address = $record[$config_ini['Columns']['street_number']]." ".$record[$config_ini['Columns']['street_name']];
	}

	$columns['listing_number'] = $record[$config_ini['Columns']['listing_number']];
	$columns['idx_resource'] = $config_ini['Definition']['resource'];
	$columns['idx_class'] = $config_ini['Definition']['class'];
	$columns['address'] = $address;

	if (isset($config_ini['Columns']['county'])) $columns['county'] = $record[$config_ini['Columns']['county']];
	if (isset($config_ini['Columns']['city'])) $columns['city'] = $record[$config_ini['Columns']['city']];
	if (isset($config_ini['Columns']['state'])) $columns['state'] = $record[$config_ini['Columns']['state']];
	if (isset($config_ini['Columns']['zip'])) $columns['zip'] = $record[$config_ini['Columns']['zip']];
	if (isset($config_ini['Columns']['price'])) $columns['price'] = $record[$config_ini['Columns']['price']];
	if (isset($config_ini['Columns']['street_number'])) $columns['street_number'] = $record[$config_ini['Columns']['street_number']];
	if (isset($config_ini['Columns']['street_dir_prefix'])) $columns['street_dir_prefix'] = $record[$config_ini['Columns']['street_dir_prefix']];
	if (isset($config_ini['Columns']['street_name'])) $columns['street_name'] = $record[$config_ini['Columns']['street_name']];
	if (isset($config_ini['Columns']['unit_number'])) $columns['unit_number'] = $record[$config_ini['Columns']['unit_number']];
	if (isset($config_ini['Columns']['status'])) $columns['status'] = $record[$config_ini['Columns']['status']];
	if (isset($config_ini['Columns']['acres'])) $columns['acres'] = $record[$config_ini['Columns']['acres']];
	if (isset($config_ini['Columns']['num_units'])) $columns['num_units'] = $record[$config_ini['Columns']['num_units']];
	if (isset($config_ini['Columns']['list_agent_id'])) $columns['list_agent_id'] = $record[$config_ini['Columns']['list_agent_id']];
	if (isset($config_ini['Columns']['co_list_agent_id'])) $columns['co_list_agent_id'] = $record[$config_ini['Columns']['co_list_agent_id']];
	if (isset($config_ini['Columns']['list_office_id'])) $columns['list_office_id'] = $record[$config_ini['Columns']['list_office_id']];
	if (isset($config_ini['Columns']['co_list_office_id'])) $columns['co_list_office_id'] = $record[$config_ini['Columns']['co_list_office_id']];
	if (isset($config_ini['Columns']['selling_agent_id'])) $columns['selling_agent_id'] = $record[$config_ini['Columns']['selling_agent_id']];
	if (isset($config_ini['Columns']['selling_office_id'])) $columns['selling_office_id'] = $record[$config_ini['Columns']['selling_office_id']];
	if (isset($config_ini['Columns']['date_listed'])) $columns['date_listed'] = date_cleanup($record[$config_ini['Columns']['date_listed']]);
	if (isset($config_ini['Columns']['date_expired'])) $columns['date_expired'] = date_cleanup($record[$config_ini['Columns']['date_expired']]);
	if (isset($config_ini['Columns']['date_price'])) $columns['date_price'] = date_cleanup($record[$config_ini['Columns']['date_price']]);
	if (isset($config_ini['Columns']['date_status'])) $columns['date_status'] = date_cleanup($record[$config_ini['Columns']['date_status']]);
	if (isset($config_ini['Columns']['date_contract'])) $columns['date_contract'] = date_cleanup($record[$config_ini['Columns']['date_contract']]);
	if (isset($config_ini['Columns']['date_sold'])) $columns['date_sold'] = date_cleanup($record[$config_ini['Columns']['date_sold']]);
	if (isset($config_ini['Columns']['date_updated'])) $columns['date_updated'] = date_cleanup($record[$config_ini['Columns']['date_updated']]);
	if (isset($config_ini['Columns']['date_photo'])) $columns['date_photo'] = date_cleanup($record[$config_ini['Columns']['date_photo']]);
	if (isset($config_ini['Columns']['date_off_market'])) $columns['date_off_market'] = date_cleanup($record[$config_ini['Columns']['date_off_market']]);
	if (isset($config_ini['Columns']['sold_price'])) $columns['sold_price'] = $record[$config_ini['Columns']['sold_price']];
	if (isset($config_ini['Columns']['photo_count'])) $columns['photo_count'] = $record[$config_ini['Columns']['photo_count']];
	if (isset($config_ini['Columns']['has_garage'])) $columns['has_garage'] = $has_garage;
	if (isset($config_ini['Columns']['has_basement'])) $columns['has_basement'] = $has_basement;
	if (isset($config_ini['Columns']['has_covenants'])) $columns['has_covenants'] = $has_covenants;
	if (isset($config_ini['Columns']['has_easement'])) $columns['has_easement'] = $has_easement;
	if (isset($config_ini['Columns']['has_flood_zone'])) $columns['has_flood_zone'] = $has_flood_zone;
	if (isset($config_ini['Columns']['is_auction'])) $columns['is_auction'] = $is_auction;
	if (isset($config_ini['Columns']['is_short_sale'])) $columns['is_short_sale'] = $is_short_sale;
	if (isset($config_ini['Columns']['condo_name'])) $columns['condo_name'] = $record[$config_ini['Columns']['condo_name']];
	if (isset($config_ini['Columns']['electric_company'])) $columns['electric_company'] = $record[$config_ini['Columns']['electric_company']];
	if (isset($config_ini['Columns']['zoning'])) $columns['zoning'] = $record[$config_ini['Columns']['zoning']];
	if (isset($config_ini['Columns']['village_dist_locale'])) $columns['village_dist_locale'] = $record[$config_ini['Columns']['village_dist_locale']];
	if (isset($config_ini['Columns']['phone_company'])) $columns['phone_company'] = $record[$config_ini['Columns']['phone_company']];
	if (isset($config_ini['Columns']['title_company'])) $columns['title_company'] = $record[$config_ini['Columns']['title_company']];
	if (isset($config_ini['Columns']['parcel_number'])) $columns['parcel_number'] = $record[$config_ini['Columns']['parcel_number']];
	if (isset($config_ini['Columns']['development'])) $columns['development'] = $record[$config_ini['Columns']['development']];
	if (isset($config_ini['Columns']['baths_total'])) $columns['baths_total'] = $record[$config_ini['Columns']['baths_total']];
	if (isset($config_ini['Columns']['baths_full'])) $columns['baths_full'] = $record[$config_ini['Columns']['baths_full']];
	if (isset($config_ini['Columns']['baths_half'])) $columns['baths_half'] = $record[$config_ini['Columns']['baths_half']];
	if (isset($config_ini['Columns']['baths_quarter'])) $columns['baths_quarter'] = $record[$config_ini['Columns']['baths_quarter']];
	if (isset($config_ini['Columns']['baths_three_quarter'])) $columns['baths_three_quarter'] = $record[$config_ini['Columns']['baths_three_quarter']];
	if (isset($config_ini['Columns']['beds_total'])) $columns['beds_total'] = $record[$config_ini['Columns']['beds_total']];
	if (isset($config_ini['Columns']['year_built'])) $columns['year_built'] = $record[$config_ini['Columns']['year_built']];
	if (isset($config_ini['Columns']['floor_number'])) $columns['floor_number'] = $record[$config_ini['Columns']['floor_number']];
	if (isset($config_ini['Columns']['finished_sqft'])) $columns['finished_sqft'] = $record[$config_ini['Columns']['finished_sqft']];
	if (isset($config_ini['Columns']['lot_sqft'])) $columns['lot_sqft'] = $record[$config_ini['Columns']['lot_sqft']];
	if (isset($config_ini['Columns']['garage_capacity'])) $columns['garage_capacity'] = $record[$config_ini['Columns']['garage_capacity']];
	if (isset($config_ini['Columns']['hoa_fee'])) $columns['hoa_fee'] = $record[$config_ini['Columns']['hoa_fee']];
	if (isset($config_ini['Columns']['hoa_frequency'])) $columns['hoa_frequency'] = $record[$config_ini['Columns']['hoa_frequency']];
	if (isset($config_ini['Columns']['school_elementry'])) $columns['school_elementry'] = $record[$config_ini['Columns']['school_elementry']];
	if (isset($config_ini['Columns']['school_middle'])) $columns['school_middle'] = $record[$config_ini['Columns']['school_middle']];
	if (isset($config_ini['Columns']['school_high'])) $columns['school_high'] = $record[$config_ini['Columns']['school_high']];
	if (isset($config_ini['Columns']['construction_status'])) $columns['construction_status'] = $record[$config_ini['Columns']['construction_status']];
	if (isset($config_ini['Columns']['has_virtual_tour'])) $columns['has_virtual_tour'] = $has_virtual_tour;
	if (isset($config_ini['Columns']['latitude'])) $columns['latitude'] = $record[$config_ini['Columns']['latitude']];
	if (isset($config_ini['Columns']['longitude'])) $columns['longitude'] = $record[$config_ini['Columns']['longitude']];
	if (isset($config_ini['Columns']['listing_class'])) $columns['listing_class'] = $record[$config_ini['Columns']['listing_class']];
	if (isset($config_ini['Columns']['listing_type'])) $columns['listing_type'] = $record[$config_ini['Columns']['listing_type']];
	if (isset($config_ini['Columns']['total_stories'])) $columns['total_stories'] = $record[$config_ini['Columns']['total_stories']];
	if (isset($config_ini['Columns']['school_district'])) $columns['school_district'] = $record[$config_ini['Columns']['school_district']];
	if (isset($config_ini['Columns']['owner_name'])) $columns['owner_name'] = $record[$config_ini['Columns']['owner_name']];
	if (isset($config_ini['Columns']['owner_phone'])) $columns['owner_phone'] = $record[$config_ini['Columns']['owner_phone']];
	if (isset($config_ini['Columns']['has_waterfront'])) $columns['has_waterfront'] = $has_waterfront;




	$sth = SQL_QUERY("SELECT listing_number from ".$mls_ini['MLS']['database'].".listings where listing_number='".$record[$config_ini['Columns']['listing_number']]."' limit 1");
	if (SQL_NUM_ROWS($sth) == 0) {

		$sql = "INSERT INTO ".$mls_ini['MLS']['database'].".listings (";
		$sql .= "`".implode("`,`", array_keys($columns))."`";
		$sql .=") values (";
		$sql .= "'".implode("', '", array_map('SQL_CLEAN', $columns))."'";
		$sql .= ")";
		SQL_QUERY($sql);
		SQL_QUERY("INSERT INTO ".$mls_ini['MLS']['database'].".listings_raw (listing_number, json_text) values ('".SQL_CLEAN($record[$config_ini['Columns']['listing_number']])."', '".SQL_CLEAN($record->toJson())."')");
		print "I";
	} else {
		$sql = "UPDATE ".$mls_ini['MLS']['database'].".listings SET ";
		$first = 0;
		foreach ($columns as $k => $v) {
			if ($k == 'office_id') continue(1);
			if (!$first) {
				$sql .= $k."='".SQL_CLEAN($v)."'";
				$first = 1;
			} else {
				$sql .= ",".$k."='".SQL_CLEAN($v)."'";
			}
		}
		$sql .= " where listing_number='".SQL_CLEAN($record[$config_ini['Columns']['listing_number']])."'";
		SQL_QUERY($sql);
		SQL_QUERY("UPDATE ".$mls_ini['MLS']['database'].".listings_raw set json_text = '".SQL_CLEAN($record->toJson())."' where listing_number = '".SQL_CLEAN($record[$config_ini['Columns']['listing_number']])."'");
		print "U";
	}
}


function address_generator($listing) {
	global $rets, $config, $options, $mls_ini, $options, $config_ini;

	$address = "";
	if ($listing[$config_ini['Columns']['street_number']] != '') $address .= $listing[$config_ini['Columns']['street_number']]." ";
	if ($listing[$config_ini['Columns']['street_dir_prefix']] != '') $address .= $listing[$config_ini['Columns']['street_dir_prefix']]." ";
	if ($listing[$config_ini['Columns']['street_name']] != '') $address .= $listing[$config_ini['Columns']['street_name']]." ";
	if ($listing[$config_ini['Columns']['street_suffix']] != '') $address .= $listing[$config_ini['Columns']['street_suffix']]." ";
	if ($listing[$config_ini['Columns']['street_dir_suffix']] != '') $address .= $listing[$config_ini['Columns']['street_dir_suffix']]." ";
	if ($listing[$config_ini['Columns']['unit_number']] != '') $address .= "#".$listing[$config_ini['Columns']['unit_number']]." ";
	$address = preg_replace("/ $/","",$address);
	return $address;
}



/* Get Current Time */
function getLogTS() {
	return date('Y-m-d H:i:s');
}

?>
