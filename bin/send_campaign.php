<?PHP
	
require_once($_SERVER['SITE_DIR']."/includes/common.php");


$shortopts = "";
$longopts = array(
	'contact_id::',
	'event_id::'
);
$options = getopt($shortopts, $longopts);

if ($options['contact_id'] > 0 && $options['event_id'] > 0) {
	print "Force Send\n";
	print "Contact ID: ".$options['contact_id']."\n";
	print "Event ID: ".$options['event_id']."\n";
	send_event($options['contact_id'],$options['event_id']);
	print "\n";
	exit;	
}



$sth = SQL_QUERY("
	select 
		cc.*
	from campaigns_rel_contacts as cc
	left join campaigns as c on c.campaign_id=cc.campaign_id
	where cc.is_onhold != 1 and cc.is_completed != 1
");
while ($campaign = SQL_ASSOC_ARRAY($sth)) {
	$next_event_id = find_next_event($campaign['campaign_id'], $campaign['contact_id']);
	print "Contact ID: ".$campaign['contact_id']."\n";
	print "Campaign ID: ".$campaign['campaign_id']."\n";
	print "Next Event: ".$next_event_id."\n";
	if ($next_event_id > 0) send_event($campaign['campaign_id'], $next_event_id);
	print "\n";

}





function send_event($contact_id, $event_id) {
	global $smarty;
	$sth = SQL_QUERY("select * from campaign_events where campaign_event_id='".$event_id."' limit 1");
	$event = SQL_ASSOC_ARRAY($sth);

	$sth = SQL_QUERY("select c.*, ce.email from contacts as c left join contact_emails as ce on ce.contact_id=c.contact_id and ce.is_primary=1 where c.contact_id='".$contact_id."' limit 1");
	$contact = SQL_ASSOC_ARRAY($sth);

	SQL_QUERY("update campaigns_rel_contacts set date_last_sent=NOW() where contact_id='".$contact_id."' and campaign_id='".$event['campaign_id']."' limit 1");

	$sth = SQL_QUERY("select u.* from contacts_rel_users as cu left join users as u on u.user_id=cu.user_id where cu.contact_id='".$contact_id."' order by is_primary desc limit 1");
	if (SQL_NUM_ROWS($sth) == 0) {
		SQL_QUERY("insert into campaign_event_rel_contacts (campaign_event_id, contact_id, date_sent, is_error, error_text, campaign_id) values ('".$event_id."','".$contact_id."',NOW(), 1, 'A primary user is not assigned to this contact.', '".$event['campaign_id']."')");
		return 0;
	}
	$from_user = SQL_ASSOC_ARRAY($sth);

	$sth = SQL_QUERY("select * from companies where company_id='".$from_user['company_id']."' limit 1");
	if (SQL_NUM_ROWS($sth) == 0) {
		SQL_QUERY("insert into campaign_event_rel_contacts (campaign_event_id, contact_id, date_sent, is_error, error_text, campaign_id) values ('".$event_id."','".$contact_id."',NOW(), 1, 'Unable to determine company of primary user.', '".$event['campaign_id']."')");
		return 0;
	}
	$company = SQL_ASSOC_ARRAY($sth);


	$custom_variables = array();
	$sth = SQL_QUERY("select var_key,var_val from company_variables where company_id='".$from_user['company_id']."'");
	while ($data = SQL_ASSOC_ARRAY($sth)) {
		$custom_variables[$data['var_key']] = $data['var_val'];
	}
	$sth = SQL_QUERY("select var_key,var_val from campaign_variables where campaign_id='".$event['campaign_id']."'");
	while ($data = SQL_ASSOC_ARRAY($sth)) {
		if (!isset($custom_variables[$data['var_key']])) $custom_variables[$data['var_key']] = $data['var_val'];
	}
	
	if ($contact['email'] == '') {
		SQL_QUERY("insert into campaign_event_rel_contacts (campaign_event_id, contact_id, date_sent, is_error, error_text, campaign_id) values ('".$event_id."','".$contact_id."',NOW(), 1, 'Contact needs a primary email selected.', '".$event['campaign_id']."')");
		return 0;
	}

	$subject = $event['event_subject'];
	$subject = str_replace('%contact.first_name%', $contact['first_name'], $subject);
	$subject = str_replace('%contact.last_name%', $contact['last_name'], $subject);
	$subject = str_replace('%contact.name%', $contact['first_name']." ".$contact['last_name'], $subject);
	$subject = str_replace('%contact.email%', $contact['email'], $subject);

	$subject = str_replace('%from.first_name%', $from_user['first_name'], $subject);
	$subject = str_replace('%from.last_name%', $from_user['last_name'], $subject);
	$subject = str_replace('%from.name%', $from_user['first_name']." ".$from_user['last_name'], $subject);
	$subject = str_replace('%from.email%', $from_user['email'], $subject);

	$subject = str_replace('%company.company_name%', $company['company_name'], $subject);
	$subject = str_replace('%company.website%', $company['website'], $subject);
	$subject = str_replace('%company.email_footer_line_1%', $company['email_footer_line_1'], $subject);
	$subject = str_replace('%company.email_footer_line_2%', $company['email_footer_line_2'], $subject);

	if ($company['custom_keyval'] != '') {
		$keyval = json_decode($company['custom_keyval']);
		foreach ($keyval as $k => $v) {
			$subject = str_replace('%company.'.$k.'%', $v, $subject);
		}
	}

	$event['event_subject'] = $subject;
	$event['event_body'] = str_replace("\n","<BR/>\n", $event['event_body']);

	SQL_QUERY("insert into campaign_event_rel_contacts (campaign_event_id, contact_id, date_sent, campaign_id) values ('".$event_id."','".$contact_id."',NOW(), '".$event['campaign_id']."')");
	$campaign_event_contact_id = SQL_INSERT_ID();

	foreach($custom_variables as $k => $v) {
		$event['event_body'] = str_replace('%'.$k.'%', $v, $event['event_body']);
	}

	$event['event_body'] = preg_replace("/%download=(\d+)\|([\w ]+)%/","<a href=\"https://www.zugent.com/services/download_campaign_file.php?uid=".$contact_id."&cid=".$campaign_event_contact_id."&caid=".$event['campaign_id']."&id=$1\">$2</a>",$event['event_body']);
	$event['event_body'] = preg_replace_callback("/%response=(.*?)%/", function($matches) use ($event, $campaign_event_contact_id, $contact_id) {
		return "https://www.zugent.com/event_response.php?uid=".$contact_id."&cid=".$campaign_event_contact_id."&caid=".$event['campaign_id']."&response=".rawurlencode($matches[1]);
	}, $event['event_body']);
	
	$smarty->assign('company', $company);
	$smarty->assign('contact', $contact);
	$smarty->assign('event', $event);
	$smarty->assign('from_user', $from_user);

	$message_body = $smarty->fetch("email_templates/standard.tpl");

	$message_body = str_replace('%company.name%', $company['company_name'], $message_body);
	$message_body = str_replace('%company.email_footer_line_1%', $company['email_footer_line_1'], $message_body);
	$message_body = str_replace('%company.email_footer_line_2%', $company['email_footer_line_2'], $message_body);
	$message_body = str_replace('%company.website%', $company['website'], $message_body);

	$message_body = str_replace('%contact.first_name%', $contact['first_name'], $message_body);
	$message_body = str_replace('%contact.last_name%', $contact['last_name'], $message_body);
	$message_body = str_replace('%contact.name%', $contact['first_name']." ".$contact['last_name'], $message_body);
	$message_body = str_replace('%contact.email%', $contact['email'], $message_body);

	$message_body = str_replace('%from.first_name%', $from_user['first_name'], $message_body);
	$message_body = str_replace('%from.last_name%', $from_user['last_name'], $message_body);
	$message_body = str_replace('%from.name%', $from_user['first_name']." ".$from_user['last_name'], $message_body);
	$message_body = str_replace('%from.email%', $from_user['email'], $message_body);

	if ($company['custom_keyval'] != '') {
		$keyval = json_decode($company['custom_keyval']);
		foreach ($keyval as $k => $v) {
			$message_body = str_replace('%company.'.$k.'%', $v, $message_body);
		}
	}

	$mail = new PHPMailer;
	$mail->IsHTML(true);
	$mail->Subject = $subject;
	$mail->Body    = $message_body;
	
	$mail->AddReplyTo($from_user['email'], $from_user['first_name']." ".$from_user['last_name']);
	$mail->SetFrom("noreply@zugent.com", $from_user['first_name']." ".$from_user['last_name']);

	if ($event['is_event_to_user']) {
		print "Sending to ".$from_user['email']."\n";
		$mail->addAddress($from_user['email'], $from_user['first_name']." ".$from_user['last_name']);
	}
	if ($event['is_event_to_client']) {
		print "Sending to ".$contact['email']."\n";
		$mail->addAddress($contact['email'], $contact['first_name']." ".$contact['last_name']);
	}	
	if (!$mail->send()) {
		print "Send Error: ".$mail->ErrorInfo."\n";
	} else {
	}
	return 1;
}


function find_next_event($campaign_id, $contact_id) {
	$sql = "
		select 
			ce.*
			, datediff(NOW(),date_added) as days_since_added
		from campaigns_rel_contacts as crc
		left join campaign_events as ce on ce.campaign_id=crc.campaign_id
		left join campaign_event_rel_contacts as cerc on cerc.campaign_event_id=ce.campaign_event_id
		where 
			ce.campaign_id='".$campaign_id."'
			and crc.contact_id='".$contact_id."'
			and cerc.date_sent is null
			and datediff(NOW(),date_added) >= ce.send_after_days
		order by ce.send_after_days ASC
	";
	$sth = SQL_QUERY($sql);
	while ($data = SQL_ASSOC_ARRAY($sth)) {
		return $data['campaign_event_id'];
	}


	return 0;
	
}




	
	
?>