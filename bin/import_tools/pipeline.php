<?PHP

require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$source_user_id = 1987;
$target_user_id = 3;

$db_source = mysqli_connect("prd-master.clzp0fmlfl17.us-west-2.rds.amazonaws.com", "asset", "postman97");
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}


// mysql -hprd-master.clzp0fmlfl17.us-west-2.rds.amazonaws.com -uasset -ppostman97 leads


$sth = mysqli_query($db_source, "
select
	p.prospect_id
	, p.first_name
	, p.last_name
	, p.home_phone
	, p.address
	, p.city
	, p.zip
	, p.county
	, p.email
	, p.media_source
	, p.disposition_id
	, p.quick_note
	, d.name as disposition_name
	, p.date_created
	, max(pm.last_modified) as last_modified
	, max(pm.date_assigned) as date_assigned
	, max(pm.last_viewed) as last_ciewed
from leads.prospects_master as pm 
left join leads.prospects as p on p.prospect_id=pm.prospect_id 
left join leads.dispositions as d on d.disposition_id=p.disposition_id 
where pm.user_id='".$source_user_id."'
group by pm.prospect_id
");
print mysqli_error($db_source);
while ($contact_data = mysqli_fetch_assoc($sth)) {

	$sth_check = SQL_QUERY("select contact_id from contacts where source_id='PIPELINE # ".SQL_CLEAN($contact_data['prospect_id'])."' limit 1");
	if (SQL_NUM_ROWS($sth_check) == 1) {
		print "Contact Already Migrated\n";
		continue(1);
	}

	$contact_data['address'] = str_replace("\r\n",", ", $contact_data['address']);
	$contact_data['address'] = str_replace("\n",", ", $contact_data['address']);

	$disposition = map_disposition_id($contact_data['disposition_id']);

	$sth_i = SQL_QUERY("
		insert into zugent.contacts (
			first_name
			, last_name
			, date_created
			, date_updated
			, street_address
			, city
			, state
			, zip
			, county
			, summary
			, bucket_id
			, source_id
		) values (
			'".SQL_CLEAN($contact_data['first_name'])."'
			, '".SQL_CLEAN($contact_data['last_name'])."'
			, '".SQL_CLEAN($contact_data['date_created'])."'
			, '".SQL_CLEAN($contact_data['last_modified'])."'
			, '".SQL_CLEAN($contact_data['address'])."'
			, '".SQL_CLEAN($contact_data['city'])."'
			, 'WA'
			, '".SQL_CLEAN($contact_data['zip'])."'
			, '".SQL_CLEAN($contact_data['county'])."'
			, '".SQL_CLEAN($contact_data['quick_note'])."'
			, '".$disposition[0]."'
			, 'PIPELINE # ".SQL_CLEAN($contact_data['prospect_id'])."'
		)
	");
	$contact_id = SQL_INSERT_ID();

	if (intval($contact_id) == 0) {
		print "Error inserting user\n";
		exit;
	}

	SQL_QUERY("insert into contacts_rel_users (user_id, contact_id, is_primary) values ('".$target_user_id."','".$contact_id."',1)");


	if ($contact_data['email'] != '') {
		SQL_QUERY("insert into contact_emails (contact_id, email, is_primary) values ('".$contact_id."','".SQL_CLEAN($contact_data['email'])."',1)");
	}

	if ($contact_data['home_phone'] != '') {
		SQL_QUERY("insert into contact_phones (contact_id, phone_number, is_primary) values ('".$contact_id."','".preg_replace('~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '($1) $2-$3', $contact_data['home_phone'])."',1)");
	}

	print $contact_data['first_name']." ".$contact_data['last_name']."\n";

	$sth_c = mysqli_query($db_source,"
		select 
			c.date_modified
			, c.comment
			, u.first_name
			, u.last_name
			, u.username
			, c.user_id
		from leads.comments as c 
		left join leads.users as u on u.user_id=c.user_id 
		where c.prospect_id='".$contact_data['prospect_id']."'
	");	
	print mysqli_error($db_source);
	while ($comment_data = mysqli_fetch_assoc($sth_c)) {
		if ($comment_data['user_id'] == '0') $comment_data['username'] = "Automated";
		print $comment_data['username']." left a comment on ".$comment_data['date_modified']."\n";
		SQL_QUERY("
			insert into contact_comments (
				user_id
				, contact_id
				, date_added
				, comment
				, system_name
				, is_history
			) values (
				0
				, '".$contact_id."'
				, '".$comment_data['date_modified']."'
				, '".SQL_CLEAN($comment_data['comment'])."'
				, '".SQL_CLEAN($comment_data['username'])."'
				, 0
			)
		");
	}
	print "\n";	
}








function map_disposition_id($disposition_id) {
	if ($disposition_id == 66) return array(3,5,2);
	if ($disposition_id == 59) return array(3,5,1);
	if ($disposition_id == 114) return array(5,0,0);
	if ($disposition_id == 74) return array(3,6,3);
	if ($disposition_id == 75) return array(3,6,4);
	if ($disposition_id == 76) return array(3,6,5);
	if ($disposition_id == 77) return array(3,6,6);
	if ($disposition_id == 78) return array(3,6,7);
	if ($disposition_id == 79) return array(3,6,8);
	if ($disposition_id == 67) return array(3,6,9);
	if ($disposition_id == 72) return array(3,19,0);
	if ($disposition_id == 105) return array(3,20,0);
	if ($disposition_id == 70) return array(4,21,0);
	if ($disposition_id == 58) return array(2,7,0);
	if ($disposition_id == 21) return array(2,8,0);
	if ($disposition_id == 13) return array(2,9,0);
	if ($disposition_id == 14) return array(2,10,0);
	if ($disposition_id == 15) return array(2,11,0);
	if ($disposition_id == 16) return array(2,12,0);
	if ($disposition_id == 17) return array(2,13,0);
	if ($disposition_id == 60) return array(2,14,0);
	if ($disposition_id == 37) return array(2,15,0);
	if ($disposition_id == 22) return array(6,0,0);
	if ($disposition_id == 57) return array(0,18,0);
	if ($disposition_id == 99) return array(0,17,0);
	if ($disposition_id == 9) return array(0,17,0);
	if ($disposition_id == 12) return array(2,16,0);
	if ($disposition_id == 73) return array(3,20,0);
	if ($disposition_id == 80) return array(4,22,10);
	if ($disposition_id == 81) return array(4,22,11);
	if ($disposition_id == 82) return array(4,22,12);
	if ($disposition_id == 83) return array(4,22,13);
	if ($disposition_id == 84) return array(4,22,14);
	if ($disposition_id == 85) return array(4,23,15);
	if ($disposition_id == 86) return array(4,23,16);
	if ($disposition_id == 87) return array(4,23,17);
	if ($disposition_id == 88) return array(4,23,18);
	if ($disposition_id == 89) return array(4,23,19);
	if ($disposition_id == 98) return array(4,24,0);
	if ($disposition_id == 106) return array(4,25,0);
	return array(0,0,0);
}



/*


select 
	c.date_modified
	, c.comment
	, u.first_name
	, u.last_name
	, u.username
	, c.user_id
from comments as c 
left join users as u on u.user_id=c.user_id 
where c.prospect_id=868216

select
	pf.upload_date
	, pf.filename
	, pf.s3_url
	, pf.is_deleted
from prospect_files as pf
where pf.prospect_id=868216

mysql> select * from prospect_files limit 1;
+---------+-------------+---------------------+-----------------------------+--------------------------------------------------------------------------------+------------+---------+
| file_id | prospect_id | upload_date         | filename                    | s3_url                                                                         | is_deleted | user_id |
+---------+-------------+---------------------+-----------------------------+--------------------------------------------------------------------------------+------------+---------+
|       1 |      750085 | 2016-06-14 09:38:11 | ANGELI  MARIE CALDERON.docx | https://s3.amazonaws.com/assetprospectfiles/750085/ANGELI++MARIE+CALDERON.docx |          0 |       0 |
+---------+-------------+---------------------+-----------------------------+--------------------------------------------------------------------------------+------------+---------+
1 row in set (0.00 sec)

select * from transactions where prospect_id='';

mysql> select * from transactions_history limit 1;
+----------+---------+-------------------------------------------------------------------------------------------------------------+---------------------+----------------+
| thist_id | user_id | changes                                                                                                     | change_date         | transaction_id |
+----------+---------+-------------------------------------------------------------------------------------------------------------+---------------------+----------------+
|        7 |       1 | referral_company_name changed from <i>BLANK</i> to 1<br>referral_address changed from <i>BLANK</i> to 1<br> | 2010-11-08 17:02:39 |            147 |
+----------+---------+-------------------------------------------------------------------------------------------------------------+---------------------+----------------+
1 row in set (0.00 sec)




mysql> select * from appointments limit 1;
+----------------+-----------------------+-------------+------------------+---------------------+------------+---------+-------+-----------------+------------------+--------------------+--------+---------------------+--------+---------------------+-----------+-------------+------------+-------------------------+
| appointment_id | appointment_parent_id | prospect_id | appointment_type | appointment_time    | company_id | user_id | notes | completion_type | completion_notes | appointment_thread | add_by | add_date            | mod_by | mod_date            | completed | agent_notes | is_updated | appointment_original_id |
+----------------+-----------------------+-------------+------------------+---------------------+------------+---------+-------+-----------------+------------------+--------------------+--------+---------------------+--------+---------------------+-----------+-------------+------------+-------------------------+
|             53 |                     0 | 20079       | 2                | 2014-10-16 21:59:01 |          0 |       0 | Test  |               1 |                  | -53-               |      1 | 2014-10-03 22:02:16 |      0 | 2014-10-03 22:02:56 | N         |             | 1          |                    NULL |
+----------------+-----------------------+-------------+------------------+---------------------+------------+---------+-------+-----------------+------------------+--------------------+--------+---------------------+--------+---------------------+-----------+-------------+------------+-------------------------+
1 row in set (0.00 sec)


select
t.*
from transactions as t
left join prospects_master as pm on pm.prospect_id=t.prospect_id 
where
pm.user_id=1987
*/



?>
