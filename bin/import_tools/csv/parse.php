<?PHP
	
// test

require_once($_SERVER['SITE_DIR']."/includes/common.php");
ini_set('auto_detect_line_endings',TRUE);

$filename = "083-ActivityByBookOfBusiness.csv";

$deliniator = "\t";
$company_id = 3;


$fh = fopen($filename, 'r');
$head = fgetcsv($fh,0,$deliniator);
$head[count($head)-1] = trim($head[count($head)-1]);

while ($data = fgetcsv($fh, 0, $deliniator)) {
	$office_id = 0;
	$user_id = 0;
	$contact_id = 0;

	if (count($data) > count($head)) $data = array_slice($data,0,count($head));
	while (count($data) < count($head)) $data[] = '';
	$data = array_combine($head, $data);

	/* Check the Office ID */
	$sth = SQL_QUERY("select office_id from offices where company_id='".$company_id."' and name='".SQL_CLEAN($data['Office'])."' limit 1");
	if (SQL_NUM_ROWS($sth) == 0) {
		SQL_QUERY("insert into offices (company_id, name, is_active) values ('".$company_id."','".SQL_CLEAN($data['Office'])."',1)");
		$office_id = SQL_INSERT_ID();
//		print "N: ".$data['Office']." (".$office_id.")\n";
	} else {
		list($office_id) = SQL_ROW($sth);
//		print "*: ".$data['Office']." (".$office_id.")\n";
	}

	/* Check the Agents user status */
	list($last_name, $first_name) = explode(", ",$data['AgentName'],2);

//	print "First: ".$first_name."\n";
//	print "Last: ".$last_name."\n";
	
	$sth = SQL_QUERY("select user_id from users where company_id='".$company_id."' and first_name='".SQL_CLEAN($first_name)."' and last_name='".SQL_CLEAN($last_name)."' limit 1");
	if (SQL_NUM_ROWS($sth) == 0) {
		SQL_QUERY("insert into users (company_id, first_name, last_name, is_active, password) values ('".$company_id."','".SQL_CLEAN($first_name)."','".SQL_CLEAN($last_name)."',1,'')");
		$user_id = SQL_INSERT_ID();
//		print "N: ".$data['AgentName']." (".$user_id.")\n";
	} else {
		list($user_id) = SQL_ROW($sth);
//		print "*: ".$data['AgentName']." (".$user_id.")\n";
	}

	// Check if this User ID has a link to the office 
	if ($data['Office'] != '') {
		$sth = SQL_QUERY("select * from user_rel_offices where user_id='".$user_id."' and office_id='".$office_id."' limit 1");
		if (SQL_NUM_ROWS($sth) == 0) {
			print "Added to Office\n";
			SQL_QUERY("insert into user_rel_offices (user_id, office_id) values ('".$user_id."','".$office_id."')");
		}
	}
	
	// If the name ends in MA or NH its an address otherwise its a person
	if (preg_match('/, MA$/',$data['PropertyAddrOrCustomerName'])) {
		list($addy, $city, $state) = explode(", ",$data['PropertyAddrOrCustomerName'],3);
		$data['Address'] = $addy;
		$data['State'] = $state;
		$data['City'] = $city;
		$data['CustomerFirst'] = '';
		$data['CustomerLast'] = '';
	} elseif (stristr($data['PropertyAddrOrCustomerName'], "&")) {
		$customer_names = explode("&", $data['PropertyAddrOrCustomerName']);
		$cname = array_shift($customer_names);
		$cname = trim($cname);
		$cname = preg_replace("/^SPC /","",$cname);
		$cname = preg_replace("/^CAPT /","",$cname);
		$cname = preg_replace("/^Mr /","",$cname);
		$cname = preg_replace("/^Mr. /","",$cname);
		$cname = preg_replace("/^Mrs /","",$cname);
		$cname = preg_replace("/^Mrs. /","",$cname);
		print $cname."\n";
		list($customer_first, $customer_last) = explode(" ",$cname,2);
		$data['CustomerFirst'] = $customer_first;
		$data['CustomerLast'] = $customer_last;
		$data['Address'] = '';
		$data['State'] = '';
		$data['City'] = '';
	} else {
		list($customer_first, $customer_last) = explode(" ",$data['PropertyAddrOrCustomerName'],2);
		$data['CustomerFirst'] = $customer_first;
		$data['CustomerLast'] = $customer_last;
		$data['Address'] = '';
		$data['State'] = '';
		$data['City'] = '';
	}

	$sth = SQL_QUERY("select * from contacts where contact_name_in_src='".SQL_CLEAN($data['PropertyAddrOrCustomerName'])."' limit 1");	
	if (SQL_NUM_ROWS($sth) == 0) {
		// New Record
		$sth = SQL_QUERY("
			insert into contacts (
				first_name
				, last_name
				, date_created
				, date_updated
				, street_address
				, city
				, state
				, is_referral
				, contact_name_in_src
				, company_id			
			) values (
				'".SQL_CLEAN($data['CustomerFirst'])."'
				,'".SQL_CLEAN($data['CustomerLast'])."'
				,'".SQL_CLEAN(shiftdate($data['OppDate']))."'
				,'".SQL_CLEAN(shiftdate($data['OppDate']))."'
				,'".SQL_CLEAN($data['Address'])."'
				,'".SQL_CLEAN($data['City'])."'
				,'".SQL_CLEAN($data['State'])."'
				,1
				,'".SQL_CLEAN($data['PropertyAddrOrCustomerName'])."'
				,3
			)
		");
		$contact_id = SQL_INSERT_ID();
		print "Contact ID: ".$contact_id."\n";
		
		if ($data['ReferralStatus'] == 'Active') {
			$is_active = 1;
			$is_counted = 1;
			$is_complete = 0;
			$contact_referral_status = 1;
		} elseif ($data['ReferralStatus'] == "Dead - Don't Count") {
			$is_active = 0;
			$is_counted = 0;
			$is_complete = 0;
			$contact_referral_status = 5;
		} elseif ($data['ReferralStatus'] == "Pending") {
			$is_active = 1;
			$is_counted = 1;
			$is_complete = 0;
			$contact_referral_status = 2;
		} elseif ($data['ReferralStatus'] == "Listed") {
			$is_active = 1;
			$is_counted = 1;
			$is_complete = 0;
			$contact_referral_status = 3;
		} elseif ($data['ReferralStatus'] == "Closed") {
			$is_active = 1;
			$is_counted = 1;
			$is_complete = 1;
			$contact_referral_status = 4;
		} elseif ($data['ReferralStatus'] == "Dead - Counts") {
			$is_active = 1;
			$is_counted = 1;
			$is_complete = 0;
			$contact_referral_status = 5;
		}
		if ($data['CloseDate'] != '') {
			$date_complete = shiftdate($data['CloseDate']);
		} elseif ($data['DeadDate'] != '') {
			$date_complete = shiftdate($data['DeadDate']);
		} else {
			$date_complete = '';
		}
		SQL_QUERY("
			insert into contact_referral (
				referral_source
				,referral_bob
				,date_referral
				,is_active
				,is_counted
				,is_complete
				,date_complete
				,user_id
				,contact_id
				,contact_referral_status_id
			) values (
				'".SQL_CLEAN($data['ReferralSource'])."'
				,'".SQL_CLEAN($data['BookOfBusiness'])."'
				,'".SQL_CLEAN(shiftdate($data['OppDate']))."'
				,'".$is_active."'
				,'".$is_counted."'
				,'".$is_complete."'
				,'".SQL_CLEAN(shiftdate($date_complete))."'
				,'".SQL_CLEAN($user_id)."'
				,'".SQL_CLEAN($contact_id)."'
				,'".$contact_referral_status_id."'
			)
		");

		$comment = "<table class='table table-bordered table-condensed'><tr><th colspan=2>Imported Contact Details</th></tr>";
		$comment .= "<tr><td nowrap><strong>Name</strong></td><td>".$data['PropertyAddrOrCustomerName']."</td></tr>";
		$comment .= "<tr><td nowrap><strong>Referral Source</strong></td><td>".$data['ReferralSource']."</td></tr>";
		$comment .= "<tr><td nowrap><strong>Status</strong></td><td>".$data['ReferralStatus']."</td></tr>";
		$comment .= "<tr><td nowrap><strong>Open Date</strong></td><td>".$data['OppDate']."</td></tr>";
		$comment .= "<tr><td nowrap><strong>Close Date</strong></td><td>".$data['CloseDate']."</td></tr>";
		$comment .= "<tr><td nowrap><strong>Dead Date</strong></td><td>".$data['DeadDate']."</td></tr>";
		$comment .= "<tr><td nowrap><strong>Book of Business</strong></td><td>".$data['BookOfBusiness']."</td></tr>";
		$comment .= "<tr><td nowrap><strong>Referral Type (Short)</strong></td><td>".$data['RefTypeShort']."</td></tr>";
		$comment .= "</table>";

		SQL_QUERY("insert into contact_comments (contact_id, date_added, comment, system_name) values ('".$contact_id."','".SQL_CLEAN(shiftdate($data['OppDate']))."','".SQL_CLEAN($comment)."','Zug Import')");
		SQL_QUERY("insert into contact_rel_offices (contact_id, office_id) values ('".$contact_id."','".$office_id."')");
		SQL_QUERY("insert into contacts_rel_users (contact_id, user_id, is_primary) values ('".$contact_id."','".$user_id."', 1)");
	}

	
	print_r($data);
	//exit;
}


function shiftdate($str) {
	list($mo,$day,$yr) = explode("/", $str, 3);
	if ($mo == '') return '';
	return $yr."-".$mo."-".$day;
}


exit;


$array = array_map('str_getcsv', file($filename));
$header = array_shift($array);
print_r($header);
exit;

array_walk($array, '_combine_array', $header);
function _combine_array(&$row, $key, $header) {
	if (count($header) != count($row)) {
		if (count($row) > count($header)) {
			$row = array_slice($row,0,count($header));
		} else {
			while (count($row) < count($header)) $row[] = '';
		}
	}
	$row = array_combine($header, $row);
}


print_r($array);
