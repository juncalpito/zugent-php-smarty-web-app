<?PHP
	
require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");


$shortopts = "";
$longopts = array(
	'to::',
	'message::'
);
$options = getopt($shortopts, $longopts);

if ($options['to'] == '' || $options['message'] == '') {
	print "Required: ./sms_test.php --to=xxxxxx --message=\"message\"\n";
	exit;	
}


$sid = "AC40abe64877e7567878f1773133cf622c";
$token = "57114e307ae4185578ee447c73220dbf";

use Twilio\Rest\Client;


$client = new Client($sid, $token);
$client->messages->create(
	$options['to'],
    array(
		'from' => '+12062029550',
        'body' => $options['message']
    )
);

	
	
?>
