<?PHP
require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$start_epoch = strtotime('-6 month');

SQL_QUERY("TRUNCATE TABLE idx_nwmls.listing_actives");

$property_type = "RESI";
fetch_listings($property_type, $start_epoch);

function fetch_listings($property_type, $start_epoch) {
	$client = new SoapClient("http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL");

	while ($start_epoch < time()) {
		print $property_type." ".date('o-m-d H:i:s',$start_epoch)." ".date('o-m-d H:i:s',strtotime('+2 day', $start_epoch))." ";
	
		$request = "
		        <EverNetQuerySpecification xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>
	                <Message>
	                	<Head><UserId>cstorey</UserId><Password>Js2!6p8nB!5</Password><SchemaName>StandardXML1_5</SchemaName></Head>
	                    <Body>
	                            <Query>
	                                    <MLS>NWMLS</MLS>
	                                    <PropertyType>$property_type</PropertyType>
	                                    <BeginDate>".date('o-m-d\TH:i:s',$start_epoch)."</BeginDate>
	                                    <EndDate>".date('o-m-d\TH:i:s',strtotime('+2 day', $start_epoch))."</EndDate>
	                            </Query>
                                <Filter>LN,ST</Filter>
	                    </Body>
	                </Message>
		        </EverNetQuerySpecification>
		";
		$listings = $client->RetrieveListingData(array('v_strXmlQuery' => $request))->RetrieveListingDataResult;
		$xml = simplexml_load_string($listings);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);
		$listings = array_pop($array);
		if (isset($listings['LN'])) {
			print "1 ";
			process_listing($listings);
		} elseif (count($listings) == 0) {
			print "0 ";
		} else {
			print count($listings)." ";
			foreach($listings as $listing) {
				process_listing($listing);
			}
		}
		print "\n";
		$start_epoch = strtotime('+2 day', $start_epoch);
	}
}

function process_listing($listing) {
	SQL_QUERY("INSERT INTO idx_nwmls.listing_actives SET listing_number='".SQL_CLEAN($listing['LN'])."',status='".SQL_CLEAN($listing['ST'])."'");
	print ".";
}
	
?>