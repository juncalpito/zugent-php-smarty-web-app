<?PHP
require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$client = new SoapClient("http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL");
print date('o-m-d H:i:s',time())." ";
$request = "
        <EverNetQuerySpecification xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>
            <Message>
            	<Head><UserId>cstorey</UserId><Password>Js2!6p8nB!5</Password><SchemaName>StandardXML1_1</SchemaName></Head>
                <Body>
                        <Query><MLS>NWMLS</MLS></Query>
                        <Filter />
                </Body>
            </Message>
        </EverNetQuerySpecification>
";
$members = $client->RetrieveMemberData(array('v_strXmlQuery' => $request))->RetrieveMemberDataResult;
$xml = simplexml_load_string($members);
$json = json_encode($xml);
$array = json_decode($json,TRUE);
$members = array_pop($array);

print count($members)." ";
foreach($members as $member) {
	process_member($member);
}
print "\n";

function process_member($member) {
	SQL_QUERY("
		REPLACE INTO idx_nwmls.agents SET 
		agent_id='".SQL_CLEAN($member['MemberMLSID'])."'
		, first_name='".SQL_CLEAN($member['FirstName'])."'
		, last_name='".SQL_CLEAN($member['LastName'])."'
		, office_id='".SQL_CLEAN($member['OfficeMLSID'])."'
		, office_name='".SQL_CLEAN($member['OfficeName'])."'
		, phone='".SQL_CLEAN($member['OfficeAreaCode'].$member['OfficePhone'])."'
		, extension='".SQL_CLEAN($member['OfficePhoneExtension'])."'
	");
	print ".";
}
	
?>