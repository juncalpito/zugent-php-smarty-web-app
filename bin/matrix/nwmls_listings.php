<?PHP
require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$start_epoch = strtotime('-6 month');

$property_type = "RESI";
fetch_listings($property_type, $start_epoch);
//fetch_listing($property_type, "1001384");

function fetch_listings($property_type, $start_epoch) {
	$client = new SoapClient("http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL");

	while ($start_epoch < time()) {
		print $property_type." ".date('o-m-d H:i:s',$start_epoch)." ".date('o-m-d H:i:s',strtotime('+1 day', $start_epoch))." ";
	
		$request = "
		        <EverNetQuerySpecification xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>
	                <Message>
	                	<Head><UserId>cstorey</UserId><Password>Js2!6p8nB!5</Password><SchemaName>StandardXML1_1</SchemaName></Head>
	                    <Body>
	                            <Query>
	                                    <MLS>NWMLS</MLS>
	                                    <PropertyType>$property_type</PropertyType>
	                                    <BeginDate>".date('o-m-d\TH:i:s',$start_epoch)."</BeginDate>
	                                    <EndDate>".date('o-m-d\TH:i:s',strtotime('+1 day', $start_epoch))."</EndDate>
	                            </Query>
	                            <Filter />
	                    </Body>
	                </Message>
		        </EverNetQuerySpecification>
		";
		$listings = $client->RetrieveListingData(array('v_strXmlQuery' => $request))->RetrieveListingDataResult;
		$xml = simplexml_load_string($listings);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);
		$listings = array_pop($array);
		if (isset($listings['LN'])) {
			print "1 ";
			process_listing($listings);
		} elseif (count($listings) == 0) {
			print "0 ";
		} else {
			print count($listings)." ";
			foreach($listings as $listing) {
				process_listing($listing);
			}
		}
		print "\n";
		$start_epoch = strtotime('+1 day', $start_epoch);
	}
}


function fetch_listing($property_type, $listing_number) {
	$client = new SoapClient("http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL");

	print $property_type." ".$listing_number." ";
	$request = "
	        <EverNetQuerySpecification xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>
                <Message>
                	<Head><UserId>cstorey</UserId><Password>Js2!6p8nB!5</Password><SchemaName>StandardXML1_1</SchemaName></Head>
                    <Body>
                            <Query>
                                    <MLS>NWMLS</MLS>
                                    <PropertyType>$property_type</PropertyType>
									<ListingNumber>$listing_number</ListingNumber>
                            </Query>
                            <Filter />
                    </Body>
                </Message>
	        </EverNetQuerySpecification>
	";
	$listings = $client->RetrieveListingData(array('v_strXmlQuery' => $request))->RetrieveListingDataResult;
	$xml = simplexml_load_string($listings);
	$json = json_encode($xml);
	$array = json_decode($json,TRUE);
	$listings = array_pop($array);
	if (isset($listings['LN'])) {
		print "1 ";
		process_listing($listings);
	} elseif (count($listings) == 0) {
		print "0 ";
	} else {
		print count($listings)." ";
		foreach($listings as $listing) {
			process_listing($listing);
		}
	}
	print "\n";
}

function process_listing($listing) {
    $address = "";
    if ($listing['HSN'] != '') $address .= $listing['HSN']." ";
    if ($listing['DRP'] != '') $address .= $listing['DRP']." ";
    if ($listing['STR'] != '') $address .= $listing['STR']." ";
    if ($listing['SSUF'] != '') $address .= $listing['SSUF']." ";
    if ($listing['DRS'] != '') $address .= $listing['DRS']." ";
    if ($listing['UNT'] != '') $address .= "#".$listing['UNT']." ";
    $address = preg_replace("/ $/","",$address);

	$has_garage = ($listing['GSP'] > 0 ? 1 : 0);
	$has_basement = ($listing['BSM'] != '' ? 1 : 0);
	$has_easements = ($listing['ESM'] != '' ? 1 : 0);
	$is_bank_owned = ($listing['BREO'] != '' ? 1 : 0);
	$has_virtual_tour = ($listing['VIRT'] != '' ? 1: 0);
	$is_waterfront = ($listing['WFT'] != '' ? 1: 0);
	if (is_array($listing['EL'])) $listing['EL'] = implode(", ", $listing['EL']);
	if (is_array($listing['JH'])) $listing['JH'] = implode(", ", $listing['JH']);
	if (is_array($listing['SH'])) $listing['SH'] = implode(", ", $listing['SH']);

	SQL_QUERY("
		replace into idx_nwmls.listings set
		listing_number='".SQL_CLEAN($listing['LN'])."'
		,address='".SQL_CLEAN($address)."'
		,county='".SQL_CLEAN($listing['COU'])."'
		,city='".SQL_CLEAN($listing['CIT'])."'
		,state='".SQL_CLEAN($listing['STA'])."'
		,zip='".SQL_CLEAN($listing['ZIP'])."'
		,price='".SQL_CLEAN($listing['LP'])."'
		,street_number='".SQL_CLEAN($listing['HSN'])."'
		,street_dir_prefix='".SQL_CLEAN($listing['DRP'])."'
		,street_name='".SQL_CLEAN($listing['STR'])."'
		,street_suffix='".SQL_CLEAN($listing['SSUF'])."'
		,street_dir_suffix='".SQL_CLEAN($listing['DRS'])."'
		,unit_number='".SQL_CLEAN($listing['UNT'])."'
		,status='".SQL_CLEAN($listing['ST'])."'
		,acres='".SQL_CLEAN(round($listing['LSF']*43560,2))."'
		,num_units='".SQL_CLEAN($listing['NOU'])."'
		,list_agent_id='".SQL_CLEAN($listing['LAG'])."'
		,list_office_id='".SQL_CLEAN($listing['LO'])."'
		,co_list_agent_id='".SQL_CLEAN($listing['CLA'])."'
		,selling_agent_id='".SQL_CLEAN($listing['SAG'])."'
		,selling_office_id='".SQL_CLEAN($listing['SO'])."'
		,date_listed='".SQL_CLEAN($listing['LD'])."'
		,date_expired='".SQL_CLEAN($listing['XP'])."'
		,date_status='".SQL_CLEAN($listing['SDT'])."'
		,date_pending='".SQL_CLEAN($listing['PDR'])."'
		,date_contigent='".SQL_CLEAN($listing['CTDT'])."'
		,date_sold='".SQL_CLEAN($listing['CLO'])."'
		,date_updated='".SQL_CLEAN($listing['UD'])."'
		,is_new_construction='".SQL_CLEAN($listing['NC'])."'
		,sold_price='".SQL_CLEAN($listing['SP'])."'
		,photo_count='".SQL_CLEAN($listing['PIC'])."'
		,has_garage='".SQL_CLEAN($has_garage)."'
		,has_basement='".SQL_CLEAN($has_basement)."'
		,has_easement='".SQL_CLEAN($has_easements)."'
		,is_bank_owned='".SQL_CLEAN($is_bank_owned)."'
		,electric_company='".SQL_CLEAN($listing['POC'])."'
		,zoning='".SQL_CLEAN($listing['ZNC'])."'
		,parcel_number='".SQL_CLEAN($listing['TAX'])."'
		,baths_total='".SQL_CLEAN($listing['BTH'])."'
		,beds_total='".SQL_CLEAN($listing['BR'])."'
		,year_built='".SQL_CLEAN($listing['YBT'])."'
		,finished_sqft='".SQL_CLEAN($listing['SFF'])."'
		,lot_sqft='".SQL_CLEAN($listing['LSF'])."'
		,garage_capacity='".SQL_CLEAN($listing['GSP'])."'
		,hoa_fee='".SQL_CLEAN($listing['HOD'])."'
		,school_elementary='".SQL_CLEAN($listing['EL'])."'
		,school_middle='".SQL_CLEAN($listing['JH'])."'
		,school_high='".SQL_CLEAN($listing['SH'])."'
		,has_virtual_tour='".SQL_CLEAN($has_virtual_tour)."'
		,latitude='".SQL_CLEAN($listing['LAT'])."'
		,longitude='".SQL_CLEAN($listing['LONG'])."'
		,listing_type='".SQL_CLEAN($listing['PTYP'])."'
		,total_stories='".SQL_CLEAN($listing['SIB'])."'
		,school_district='".SQL_CLEAN($listing['SD'])."'
		,has_waterfront='".SQL_CLEAN($is_waterfront)."'
		,area_id='".SQL_CLEAN($listing['AR'])."'
	");
	SQL_QUERY("replace into idx_nwmls.listings_raw set listing_number='".SQL_CLEAN($listing['LN'])."', json_text='".SQL_CLEAN(json_encode($listing))."'");
	print ".";
}
	
	
	
?>