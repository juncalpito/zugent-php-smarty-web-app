<?PHP
require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$client = new SoapClient("http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL");
print date('o-m-d H:i:s',time())." ";
$request = "
        <EverNetQuerySpecification xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>
            <Message>
            	<Head><UserId>cstorey</UserId><Password>Js2!6p8nB!5</Password><SchemaName>StandardXML1_1</SchemaName></Head>
                <Body>
                        <Query><MLS>NWMLS</MLS></Query>
                        <Filter />
                </Body>
            </Message>
        </EverNetQuerySpecification>
";
$offices = $client->RetrieveOfficeData(array('v_strXmlQuery' => $request))->RetrieveOfficeDataResult;
$xml = simplexml_load_string($offices);
$json = json_encode($xml);
$array = json_decode($json,TRUE);
$offices = array_pop($array);
if (isset($offices['OfficeMLSID'])) {
	print "1 ";
	process_office($offices);
} elseif (count($offices) == 0) {
	print "0 ";
} else {
	print count($offices)." ";
	foreach($offices as $office) {
		process_office($office);
	}
}
print "\n";

function process_office($office) {
	if (is_array($office['WebPageAddress'])) {
		if (count($office['WebPageAddress']) > 0) {
			$office['WebPageAddress'] = array_shift($office['WebPageAddress']);
		} else {
			$office['WebPageAddress'] = '';
		}
	}
	SQL_QUERY("
		REPLACE INTO idx_nwmls.offices SET
		office_id='".SQL_CLEAN($office['OfficeMLSID'])."'
		, office_name='".SQL_CLEAN($office['OfficeName'])."'
		, address='".SQL_CLEAN($office['StreetAddress'])."'
		, city='".SQL_CLEAN($office['StreetCity'])."'
		, state='".SQL_CLEAN($office['StreetState'])."'
		, postal_code='".SQL_CLEAN($office['StreetZipCode'])."'
		, county='".SQL_CLEAN($office['StreetCounty'])."'
		, phone='".SQL_CLEAN($office['OfficeAreaCode'].$office['OfficePhone'])."'
		, fax='".SQL_CLEAN($office['FaxAreaCode'].$office['FaxPhone'])."'
		, email='".SQL_CLEAN($office['EMailAddress'])."'
		, webpage='".SQL_CLEAN($office['WebPageAddress'])."'
		, office_type='".SQL_CLEAN($office['OfficeType'])."'
	");
	print ".";
}

	
	
	
?>