<?php
require_once($_SERVER['SITE_DIR'].'/includes/PHPMailer/PHPMailerAutoload.php');
require_once($_SERVER['SITE_DIR']."/includes/common.php");
require_once($_SERVER['SITE_DIR']."/vendor/autoload.php");

$shortopts = "";
$longopts = array(
        'test::'
);
$options = getopt($shortopts, $longopts);

global $options;


print getLogTS().":#-------------------------------------------------------------\n";
print getLogTS().":# ZugEnt Transaction Alert System\n";
print getLogTS().":#-------------------------------------------------------------\n";


$sth = SQL_QUERY("select * from zugent.mls where is_active=1");
while ($mls_data = SQL_ASSOC_ARRAY($sth)) {
	$sth_office = SQL_QUERY("
		select
			GROUP_CONCAT(oro.mls_office_id) as offices
			,c.company_id
			,c.is_active
		from zugent.companies as c
		left join zugent.offices as o on o.company_id=c.company_id
		left join zugent.office_rel_offices as oro on o.office_id=oro.office_id
		where oro.mls_id='".$mls_data['mls_id']."' and c.is_active=1
		group by c.company_id
		having offices != '' and offices is not null
	");
	while ($office_data = SQL_ASSOC_ARRAY($sth_office)) {
		$office_data['offices'] = "'".str_replace(",","','",$office_data['offices'])."'";

		print getLogTS().": MLS DB: ".$mls_data['mls_database'].": Scanning Company ".$office_data['company_id']." (".$office_data['offices'].")\n";
		$sth_listings = SQL_QUERY("
			select
				l.listing_number
				,l.address
				,l.city
				,l.state
				,l.status
				,l.list_office_id
				,lo.office_name as list_office_name
				,l.list_agent_id
				,concat(la.first_name,' ',la.last_name) as list_agent_name
				,l.selling_office_id
				,so.office_name as selling_office_name
				,selling_agent_id
				,concat(sa.first_name,' ',sa.last_name) as selling_agent_name
				,date_listed
				,date_sold
			from ".$mls_data['mls_database'].".listings as l
			left join ".$mls_data['mls_database'].".agents as la on la.agent_id=l.list_agent_id
			left join ".$mls_data['mls_database'].".agents as sa on sa.agent_id=l.selling_agent_id
			left join ".$mls_data['mls_database'].".offices as lo on lo.office_id=l.list_office_id
			left join ".$mls_data['mls_database'].".offices as so on so.office_id=l.selling_office_id
			where (
				selling_office_id in (".$office_data['offices'].")
				or list_office_id in (".$office_data['offices'].")
			) and (
				date_sold > date_sub(now(),interval 5 day)
				or date_listed > date_sub(now(),interval 5 day)
			)
		");
		while ($listing_data = SQL_ASSOC_ARRAY($sth_listings)) {
			$sth_alert = SQL_QUERY("select transaction_alert_id,status from zugent.transaction_alerts where listing_number='".SQL_CLEAN($listing_data['listing_number'])."' and mls_id='".SQL_CLEAN($mls_data['mls_id'])."' and company_id='".SQL_CLEAN($office_data['company_id'])."'");
			if (SQL_NUM_ROWS($sth_alert) == 0) {
				print getLogTS().": New Alert ".$listing_data['listing_number']."\n";
				SQL_QUERY("
					insert into zugent.transaction_alerts (
					  listing_number
					  ,mls_id
					  ,address
					  ,city
					  ,state
					  ,status
					  ,list_office_id
					  ,list_office_name
					  ,list_agent_id
					  ,list_agent_name
					  ,selling_office_id
					  ,selling_office_name
					  ,selling_agent_id
					  ,selling_agent_name
					  ,date_listed
					  ,date_sold
					  ,company_id
					) values (
					  '" . SQL_CLEAN($listing_data['listing_number']) . "'
					  ,'" . SQL_CLEAN($mls_data['mls_id']) . "'
					  ,'" . SQL_CLEAN($listing_data['address']) . "'
					  ,'" . SQL_CLEAN($listing_data['city']) . "'
					  ,'" . SQL_CLEAN($listing_data['state']) . "'
					  ,'" . SQL_CLEAN($listing_data['status']) . "'
					  ,'" . SQL_CLEAN($listing_data['list_office_id']) . "'
					  ,'" . SQL_CLEAN($listing_data['list_office_name']) . "'
					  ,'" . SQL_CLEAN($listing_data['list_agent_id']) . "'
					  ,'" . SQL_CLEAN($listing_data['list_agent_name']) . "'
					  ,'" . SQL_CLEAN($listing_data['selling_office_id']) . "'
					  ,'" . SQL_CLEAN($listing_data['selling_office_name']) . "'
					  ,'" . SQL_CLEAN($listing_data['selling_agent_id']) . "'
					  ,'" . SQL_CLEAN($listing_data['selling_agent_name']) . "'
					  ,'" . SQL_CLEAN($listing_data['date_listed']) . "'
					  ,'" . SQL_CLEAN($listing_data['date_sold']) . "'
					  ,'" . SQL_CLEAN($office_data['company_id']) . "'
					)
				");
				$transaction_alert_id = SQL_INSERT_ID();

				SQL_QUERY("insert into zugent.transaction_alerts_log (transaction_alert_id,date_alert,alert_type,alert_message) values (".$transaction_alert_id.",NOW(),'Transaction Init', 'Transaction Alerting Detected for MLS # ".SQL_CLEAN($listing_data['listing_number'])." with a status of ".SQL_CLEAN($listing_data['status']).".')");
			} else {
				$alert_data = SQL_ASSOC_ARRAY($sth_alert);
				if ($alert_data['status'] != $listing_data['status']) {
					print getLogTS().": Updated Alert ".$listing_data['listing_number']." (".$alert_data['status']." -> ".$listing_data['status']."\n";
					SQL_QUERY("update zugent.transaction_alerts set is_updated=1,status='".SQL_CLEAN($listing_data['status'])."',date_sold='".SQL_CLEAN($listing_data['date_sold'])."' where transaction_alert_id='".$alert_data['transaction_alert_id']."' limit 1");
					SQL_QUERY("insert into zugent.transaction_alerts_log (transaction_alert_id,date_alert,alert_type,alert_message) values (".$alert_data['transaction_alert_id'].",NOW(),'Status Update', 'Listing MLS # ".SQL_CLEAN($listing_data['listing_number'])." has a status update from ".SQL_CLEAN($alert_data['status'])." to ".SQL_CLEAN($listing_data['status']).".')");
				}
			}
		}
	}
}

print getLogTS().":#-------------------------------------------------------------\n";
print getLogTS().":# ZugEnt Match to Existing Transactions\n";
print getLogTS().":#-------------------------------------------------------------\n";

$sth = SQL_QUERY("
	select 
		ta.*,
		(select count(*) from zugent.office_rel_offices as oro left join zugent.offices as o on o.office_id=oro.office_id where oro.mls_office_id=ta.list_office_id and o.company_id=ta.company_id limit 1) as is_listing_side,  
		(select count(*) from zugent.office_rel_offices as oro left join zugent.offices as o on o.office_id=oro.office_id where oro.mls_office_id=ta.selling_office_id and o.company_id=ta.company_id limit 1) as is_selling_side  
	from zugent.transaction_alerts as ta
	where ta.transaction_id=0
");
while ($data = SQL_ASSOC_ARRAY($sth)) {

	$sth_transaction = SQL_QUERY("select transaction_id from zugent.transactions where is_on_mls=1 and listing_number='".SQL_CLEAN($data['listing_number'])."' and company_id='".SQL_CLEAN($data['company_id'])."' limit 1");
	if (SQL_NUM_ROWS($sth_transaction) > 0) {
		list($transaction_id) = SQL_ROW($sth_transaction);
		SQL_QUERY("update zugent.transaction_alerts set transaction_id='".SQL_CLEAN($transaction_id)."' where transaction_alert_id='".SQL_CLEAN($data['transaction_alert_id'])."' limit 1");
		print getLogTS().": Alert Transaction Match: ".$data['listing_number']." (".$transaction_id.")\n";
	} else {
		print getLogTS().": No Transaction Match: ".$data['listing_number']."\n";
	}
}



print getLogTS().":#-------------------------------------------------------------\n";
print getLogTS().":# ZugEnt Send Alerts\n";
print getLogTS().":#-------------------------------------------------------------\n";

$sth = SQL_QUERY("select ta.company_id,c.company_name from zugent.transaction_alerts as ta left join zugent.companies as c on c.company_id=ta.company_id where ta.date_alert_sent is null or ta.is_updated=1 group by ta.company_id");
while ($company_data = SQL_ASSOC_ARRAY($sth)) {
	print getLogTS().": ".$company_data['company_id']." has ".$company_data['tcount']." alerts\n";
	send_alerts($company_data);
}


function send_alerts($company_data) {
	global $smarty, $options;

	$alerts = array();
	$sth_alert = SQL_QUERY("
		select
			ta.*,
			(select count(*) from zugent.office_rel_offices as oro left join zugent.offices as o on o.office_id=oro.office_id where oro.mls_office_id=ta.list_office_id and o.company_id=ta.company_id limit 1) as is_listing_side,  
			(select count(*) from zugent.office_rel_offices as oro left join zugent.offices as o on o.office_id=oro.office_id where oro.mls_office_id=ta.selling_office_id and o.company_id=ta.company_id limit 1) as is_selling_side  
		from zugent.transaction_alerts as ta
		where ta.company_id='".$company_data['company_id']."' and (ta.is_updated=1 or ta.date_alert_sent is null) 
	");
	while ($alert_data = SQL_ASSOC_ARRAY($sth_alert)) {
		if (!$options['test']) SQL_QUERY("update zugent.transaction_alerts set is_updated=0, date_alert_sent=now() where transaction_alert_id='".SQL_CLEAN($alert_data['transaction_alert_id'])."'");

		if ($alert_data['is_updated'] == 1) {
			$alerts['updated'][] = $alert_data;
		} else {
			if ($alert_data['is_listing_side']) {
				$alerts['listing'][] = $alert_data;
			} elseif ($alert_data['is_selling_side']) {
				$alerts['selling'][] = $alert_data;
			} else {
				$alerts['unknown'][] = $alert_data;
			}
		}
	}

	return 0;

	$smarty->assign('alerts', $alerts);

	$subject = "New transactions have been identified";
	$message_body = $smarty->fetch("email_templates/transaction_alert.tpl");

	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->Host = 'ssl://email-smtp.us-west-2.amazonaws.com';
	$mail->IsHTML(true);
        $mail->SMTPAuth = true;
        $mail->Username = 'AKIAJN22CVQO3RCBUGYA';
        $mail->Password = 'AnVynA9xT3YijjIlmcF2Z8DNCgPb4CYGbnRNORTsBY0B';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 443;
	$mail->Subject = $subject;
	$mail->Body    = $message_body;
	$mail->SetFrom("noreply@zugent.com", "ZugEnt Alerts");


	$coordinators = array();
	$sth_coordinators = SQL_QUERY("select user_id,first_name,last_name,email from zugent.users where company_id=3 and is_transaction_coordinator=1");
	while ($coordinator_data = SQL_ASSOC_ARRAY($sth_coordinators)) {
		if ($options['test']) {
			$mail->addAddress("razmage@gmail.com", $coordinator_data['first_name']." ".$coordinator_data['last_name']);
		} else {
			$mail->addAddress($coordinator_data['email'], $coordinator_data['first_name']." ".$coordinator_data['last_name']);
		}
		$coordinators[] = $coordinator_data;
	}
	if (!$options['test']) $mail->addBCC("razmage@gmail.com", $coordinator_data['first_name']." ".$coordinator_data['last_name']);
	if (!$mail->send()) {
		print getLogTS().": Send Error:\n".$mail->ErrorInfo."\n";
	} else {
		print getLogTS().": Send successfull.\n";
		return 1;
	}
	return 0;
}







/* Get Current Time */
function getLogTS() {
	return date('Y-m-d H:i:s');
}


?>
