<?php 

require_once(str_replace("consumer", "", $_SERVER['SITE_DIR']).'/includes/PHPMailer/PHPMailerAutoload.php');
require_once(str_replace("consumer", "", $_SERVER['SITE_DIR'])."/includes/db.php");
require_once(str_replace("consumer", "", $_SERVER['SITE_DIR'])."/includes/smarty/libs/Smarty.class.php");
$site_config = parse_ini_file(str_replace("consumer", "", $_SERVER['SITE_DIR'])."/etc/site_config.ini",true);


global $smarty;

$smarty = new Smarty();
$smarty->compile_check = true;

$smarty->template_dir = $_SERVER['SITE_DIR']."/templates/";
$smarty->compile_dir = $_SERVER['SITE_DIR']."/templates_c/";
$smarty->plugins_dir = array(str_replace("consumer", "", $_SERVER['SITE_DIR'])."/includes/smarty/libs/plugins",str_replace("consumer", "", $_SERVER['SITE_DIR'])."/includes/smarty_plugins");

$smarty->assign('recaptcha_key', $site_config['reCAPTCHA']['site_key']);
$smarty->assign( 'site_config', $site_config);

if (!SQL_CONNECT($site_config['MySQL']['Hostname'], $site_config['MySQL']['Username'], $site_config['MySQL']['Password'])) {
	$smarty->display('maintenance.tpl');
	exit;
}

session_start();

?>