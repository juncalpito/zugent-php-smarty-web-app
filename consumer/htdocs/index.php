<?php 

require_once($_SERVER['SITE_DIR']."/includes/common.php");

	// SUB DOMAIN MUST BE UNIQUE on CRM
	$sub_domain_name = explode('.', $_SERVER['HTTP_HOST']);

	$FIRST_URI = explode('/', $_SERVER['REQUEST_URI']);

	$chk_domain = SQL_QUERY("select * from company_page_settings where site_address='".SQL_CLEAN($sub_domain_name[0])."' limit 1");
		
	$domain_info = array();

	if (SQL_NUM_ROWS($chk_domain) != 0) {

		while ($data = SQL_ASSOC_ARRAY($chk_domain)) {
			$domain_info[] = $data;
		}
		
		// COMPANY MENU
		$sth2 = SQL_QUERY("select * from company_menus 
			where company_id='".SQL_CLEAN($domain_info[0]['company_id'])."' 
			order by menu_order ASC
			");

		$parent_menu = array();
		$child_menu = array();

		if (SQL_NUM_ROWS($sth2) != 0) {
			while ($data = SQL_ASSOC_ARRAY($sth2)) {
				if ($data['parent_id'] == '0') {
					$parent_menu[] = $data;
				}
				else {
					$child_menu[] = $data;
				}
			}

			$smarty->assign('parent_menu', $parent_menu);
			$smarty->assign('child_menu', $child_menu);
		}
		else {
			$smarty->assign('parent_menu', '');
			$smarty->assign('child_menu', '');
		}

		// COMPANY PAGE
		$sth = SQL_QUERY("select * from pages where company_id='".SQL_CLEAN($domain_info[0]['company_id'])."' and page_name='".SQL_CLEAN($FIRST_URI[1])."' limit 1");

		if (SQL_NUM_ROWS($sth) == 0) {
			$smarty->assign('page_template', '');
		}
		else {
			$company_template = array();
			while ($data = SQL_ASSOC_ARRAY($sth)) {
				$company_template[] = $data;
			}
			$smarty->assign('page_template', $company_template[0]['plain_code']);
		}

		// COMPANY PAGE SETTINGS
		$sth = SQL_QUERY("select * from company_page_settings where company_id='".SQL_CLEAN($domain_info[0]['company_id'])."' limit 1");

		$settings_data = array();

		if (SQL_NUM_ROWS($sth) != 0) {
			$settings_data = SQL_ASSOC_ARRAY($sth);
		}

		// COMPANY DETAILS
		$sth2 = SQL_QUERY("select * from companies where company_id='".SQL_CLEAN($domain_info[0]['company_id'])."' limit 1");

		$company_data = array();

		if (SQL_NUM_ROWS($sth2) != 0) {
			$company_data = SQL_ASSOC_ARRAY($sth2);
		}

		$smarty->assign('company_data', $company_data);
		$smarty->assign('company_settings', $settings_data);
		$smarty->assign('home_address', 'https://'.$_SERVER['HTTP_HOST'].'/');

		if ($FIRST_URI[1] == '') {
			$smarty->display("home_page.tpl");
		}
		else if ($FIRST_URI[1] == 'search') {
			$smarty->display("search_page.tpl");
		}
		else if ($FIRST_URI[1] == 'details') {
			$smarty->display("details_page.tpl");
		}
		else {
			$sth = SQL_QUERY("select * from pages where company_id='".SQL_CLEAN($domain_info[0]['company_id'])."' and page_name='".SQL_CLEAN($FIRST_URI[1])."' limit 1");

			if (SQL_NUM_ROWS($sth) != 0) {
				$smarty->display("custom_page.tpl");
			}
			else {
				$smarty->assign('home_site', $sub_domain_name[1].'.'.$sub_domain_name[2]);
				$smarty->display('not_found_error.tpl');
			}
		}

	}
	else {

		$smarty->assign('home_site', $sub_domain_name[1].'.'.$sub_domain_name[2]);

		$smarty->display('not_found_error.tpl');

		// echo($_SERVER['HTTP_HOST']);
		// echo('<br>');
		// echo($sub_domain_name[0]);
		// echo('<br>');
		// echo(str_replace("consumer", "", $_SERVER['SITE_DIR']));
		// echo('<br>');
		// echo(str_replace('/', '', $_SERVER['REQUEST_URI']));

	}

?>
