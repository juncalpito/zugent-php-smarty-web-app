<!-- BEGIN #page-container -->
<div id="header" class="header">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN header-container -->
        <div class="header-container">
            <!-- BEGIN navbar-header -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="header-logo">
                    <a href="javascript:;">
                        <span class="brand"></span>
                        <span>Color</span>Admin
                        <small>e-commerce frontend theme</small>
                    </a>
                </div>
            </div>
            <!-- END navbar-header -->
            <!-- BEGIN header-nav -->
            {if $parent_menu ne ''}
            <div class="header-nav">
                <div class=" collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav">
                        {foreach item=parent from=$parent_menu}
                            {if $parent.has_child eq '1'}
                            <li class="dropdown dropdown-hover">
                                {if $parent.is_custom_page eq 1}
                                <a href="{$home_address}{$parent.menu_link}" data-toggle="dropdown">
                                {else}
                                <a href="{$parent.menu_link}" data-toggle="dropdown">
                                {/if}
                                    {$parent.menu_label} 
                                    <i class="fa fa-angle-down"></i> 
                                    <span class="arrow top"></span>
                                </a>
                                <ul class="dropdown-menu">
                                {foreach item=child from=$child_menu}
                                    {if $child.parent_id eq $parent.menu_id}
                                        {if $child.is_custom_page eq 1}
                                        <li><a href="{$home_address}{$child.menu_link}">{$child.menu_label}</a></li>
                                        {else}
                                        <li><a href="{$child.menu_link}">{$child.menu_label}</a></li>
                                        {/if}
                                    {/if}
                                {/foreach}
                                </ul>
                            </li>
                            {else}
                                {if $parent.is_custom_page eq 1}
                                <li><a href="{$home_address}{$parent.menu_link}">{$parent.menu_label}</a></li>
                                {else}
                                <li><a href="{$parent.menu_link}">{$parent.menu_label}</a></li>
                                {/if}
                            {/if}
                          </li>
                        {/foreach}
                    </ul>
                </div>
            </div>
            {else}
            Menu Not Set
            {/if}
            <!-- END header-nav -->
            <!-- BEGIN header-nav -->
            <div class="header-nav">
                <ul class="nav pull-right">
                    <li>
                        <a href="#modal-login" data-toggle="modal">
                            <i class="fa fa-sign-in"></i>
                            <span class="">Sign in</span>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#modal-signin" data-toggle="modal">
                            <span class="">Join</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END header-nav -->
        </div>
        <!-- END header-container -->
    </div>
    <!-- END container -->
</div>