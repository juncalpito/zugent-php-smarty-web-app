<?php 

require_once($_SERVER['SITE_DIR']."/includes/common.php");


if(isset($_POST['lat']) && isset($_POST['lon'])){

	if(!isset($_POST['driving'])) {
		$date = gmdate("Y-m-d H:i:s");

		$sth = SQL_QUERY("
			insert into user_locations set ".
			"latitude='".SQL_CLEAN($_POST['lat']).
			"', longitude='".SQL_CLEAN($_POST['lon']).
			"', date_collected=NOW(), user_id='".SQL_CLEAN($_SESSION['user']['user_id']).
			"'");

	}

	$date = gmdate("Y-m-d H:i:s");

	$sth = SQL_QUERY("
			update users set ".
				"last_latitude='".SQL_CLEAN($_POST['lat']).
				"', last_longitude='".SQL_CLEAN($_POST['lon']).
				"' where user_id='".SQL_CLEAN($_SESSION['user']['user_id']).
				"'");

	echo json_encode(array("lat"=>$_POST["lat"], "lon"=>$_POST["lon"], "user_id"=>$_SESSION["user_id"], "date_collected"=>$date));
}
else {
	if ($is_ajax) {
		echo json_encode(false);
	}
	else {
		header("location: /dashboard.php?code=1xdv7");
	}
}

?>