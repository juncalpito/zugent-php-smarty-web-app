<?php 

require_once($_SERVER['SITE_DIR']."/htdocs/services/send_sms.php");


if (isset($_GET['request'])) {

	if (empty($_GET['request'])) 
		header('location: /404');

	$temp = explode('-', $_GET['request']);
	$sth = SQL_QUERY("select * from agent_requests where agent_request_id=".SQL_CLEAN($temp[1])." and request='".SQL_CLEAN($temp[0])."'");

	if (SQL_NUM_ROWS($sth) == 0) {
		echo "Request not valid";
		exit; 
	}

	$request_data = SQL_ASSOC_ARRAY($sth);

	// Check if the contact has been taken or doesn't exist
	$sth = SQL_QUERY("select * from contacts where contact_id=".SQL_CLEAN($request_data['contact_id'])." and company_id=0");

	if (SQL_NUM_ROWS($sth) == 0) {
		echo "Contact has already been assigned";
		exit; 
	}

	$sth = SQL_QUERY("select * from users where user_id=".SQL_CLEAN($request_data['user_id']));
	$user_data = SQL_ASSOC_ARRAY($sth);

	SQL_QUERY("update contacts set company_id=".SQL_CLEAN($user_data['company_id'])." where contact_id=".SQL_CLEAN($request_data['contact_id']));
	SQL_QUERY("update contact_referral set user_id=".SQL_CLEAN($user_data['user_id'])." where contact_id=".SQL_CLEAN($request_data['contact_id']));

	$cond1 = SQL_QUERY("insert into contacts_rel_users set is_primary=1, user_id=".SQL_CLEAN($user_data['user_id']).", contact_id=".SQL_CLEAN($request_data['contact_id']));
	$cond2 = SQL_QUERY("update agent_requests set accepted=1 where agent_request_id=".SQL_CLEAN($temp[1]));

	// Add referral log
	add_user_log("Agent (".$request_data['user_id'].") accepted a request", "requests", array("importance" => "Info", "action" => "Add") );

	if ($cond1 && $cond2) {
		// add notification
		// id, title, notification, url_action
		add_notification($user_data['user_id'], "New Contact", "You accepted a new contact", $_SERVER['HTTP_HOST']."/contacts_edit.php?id=".$request_data['contact_id']);

		// Will change to display in website 
		// header("location: https://www.google.com/maps/dir/?api=1&destination=".$request_data['latitude'].",".$request_data['longitude']."");
		header("location: /drive.php");
	}
}
else {
	header("location: /request_agent.php?code=1x007");
}

?>