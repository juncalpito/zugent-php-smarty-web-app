<?php 

// require_once($_SERVER['SITE_DIR']."/htdocs/services/send_sms.php");
require_once($_SERVER['SITE_DIR']."/includes/common.php");

header('Access-Control-Allow-Origin: *');


if (isset($_POST['all_agents'])) {
	
	$agents = array();
	$least_distance = array();
	$temp = array();

	$sth = SQL_QUERY("select u.user_id, u.is_profile_photo, u.first_name, u.last_name, 
		ul.latitude as last_latitude, ul.longitude as last_longitude, u.phone_mobile, comp.company_name 
		from users as u 
		left join user_locations as ul on u.user_id=ul.user_id 
		left join companies as comp on u.company_id=comp.company_id 
		where ul.date_collected > DATE_SUB(NOW(), INTERVAL 1 DAY) and 
		(u.is_accepting_outside_referral = 1 or u.company_id = ".SQL_CLEAN($_SESSION['user']['company_id']).")
		order by ul.date_collected DESC");

	while($data = SQL_ASSOC_ARRAY($sth)) {
		$agents[] = $data;
	}

	// echo "<pre>";
	// print_r(array("error" => false, "count" => count($agents), "agents" => $agents));
	// echo "</pre>";
	// die('TESTING');

	echo json_encode(array("error" => false, "count" => count($agents), "agents" => $agents));

}
else {
	header("location: /dashboard.php?code=1x007");
}

?>