<?php

require_once($_SERVER['SITE_DIR']."/includes/common.php");

$sth = SQL_QUERY("
	select 
		* 
	from contacts_rel_users 
	where user_id='".SQL_CLEAN($_SESSION['user_id'])."' 
	and is_deleted != 1 
	and contact_id='".SQL_CLEAN($_POST['id'])."' 
	limit 1
");
if (SQL_NUM_ROWS($sth) == 0) {
	print "0";
	exit;
}

$sth = SQL_QUERY("select * from campaigns_rel_contacts where contact_id='".SQL_CLEAN($_POST['id'])."' and campaign_id='".SQL_CLEAN($_POST['campaign'])."' limit 1");
if (SQL_NUM_ROWS($sth) != 0) {
	print "0";
	exit;
}

SQL_QUERY("
	insert into campaigns_rel_contacts 
	(
		user_id
		, contact_id
		, date_added
		, campaign_id
	) values (
		'".SQL_CLEAN($_SESSION['user_id'])."'
		, '".SQL_CLEAN($_POST['id'])."'
		, NOW()
		, '".SQL_CLEAN($_POST['campaign'])."'
	)
");

$campaign_id = SQL_INSERT_ID();
	
// Add campaign log
add_user_log("Added a campaign (".$campaign_id.")", "campaigns", array("importance" => "Info", "action" => "Add") );

print "1";
exit;

	
?>