<?php

require_once($_SERVER['SITE_DIR']."/includes/common.php");

check_company_page_access('drive');

auth(false, true);

$smarty->assign('footer_js', 'includes/footers/find_agent_footer.tpl');
$smarty->display('find_agent.tpl');

?>