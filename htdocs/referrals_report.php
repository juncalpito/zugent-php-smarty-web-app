<?php

require_once($_SERVER['SITE_DIR']."/includes/common.php");

check_company_page_access('referrals');

auth();

$sth = "";

if($_SESSION['user']['is_admin'] == 1) {
	$sth = SQL_QUERY("select c.*, cr.is_active, cr.is_complete, cr.is_counted, cr.referral_source, cr.date_referral, cr.date_complete
						, CONCAT(u.first_name,' ', u.last_name) as user_full_name 
						, CONCAT(c.first_name,' ', c.last_name) as contact_name 
						, u.user_id 
						from contacts as c 
						inner join contacts_rel_users as cru on cru.contact_id=c.contact_id 
						left join users as u on cru.user_id=u.user_id 
						left join contact_referral as cr on cr.contact_id=c.contact_id  
						where c.is_referral=1 and u.company_id=".SQL_CLEAN($_SESSION['user']['company_id'])." 
						order by c.date_created DESC");
}
else {
	$sth = SQL_QUERY("select c.*, cr.is_active, cr.is_complete, cr.is_counted, cr.referral_source, cr.date_referral, cr.date_complete
						, CONCAT(u.first_name,' ', u.last_name) as user_full_name
						, CONCAT(c.first_name,' ', c.last_name) as contact_name 
						, u.user_id from contacts as c 
						inner join contacts_rel_users as cru on cru.contact_id=c.contact_id 
						left join users as u on cru.user_id=u.user_id 
						left join contact_referral as cr on cr.contact_id=c.contact_id 
						where c.is_referral=1 and cru.user_id=".SQL_CLEAN($_SESSION['user_id'])." 
						and u.company_id=".SQL_CLEAN($_SESSION['user']['company_id'])." 
						order by c.date_created DESC, contact_name ASC");
}

$contact_referrals = array();
$filters = array();

$referrals_generated = 0;
$referrals_completed = 0;
$referrals_deactive = 0;
$total_referrals = SQL_NUM_ROWS($sth);

while ($data = SQL_ASSOC_ARRAY($sth)) {
	if ($data['is_active'] == 1)
		$data['status'] = "Active";
	else if ($data['is_active'] == 0 && $data['is_complete'] == 1) {
		$data['status'] = "Completed";

		if(strtotime($data['date_referral']) > strtotime("-30 days")) 
			$referrals_completed++;
	}
	else if ($data['is_active'] == 0 && $data['is_complete'] == 0) {
		$data['status'] = "Dead";

		if(strtotime($data['date_referral']) > strtotime("-30 days")) 
			$referrals_deactive++;
	}

	if(strtotime($data['date_referral']) > strtotime("-30 days")) 
		$referrals_generated++;

	if(trim($data['user_full_name']) == "") {
		$data['user_full_name'] = "Unassigned";
	}

	if(trim($data['contact_name']) == "") {
		$data['contact_name'] = "No Name";
	}

	if ($data['is_counted'] == 0)
		$data['status'] .= " - not counted";
	$contact_referrals[] = $data;
}

if($_SESSION['user']['is_admin'] == 1 || $_SESSION['user']['is_superadmin'] == 1) {
	$sth = SQL_QUERY("select distinct CONCAT(u.first_name,' ', u.last_name) as user_full_name , u.user_id from contacts as c 
						inner join contacts_rel_users as cr on cr.contact_id=c.contact_id 
						left join users as u on cr.user_id=u.user_id where c.is_referral=1");
} 
else {
	$sth = SQL_QUERY("select distinct CONCAT(u.first_name,' ', u.last_name) as user_full_name, u.user_id from contacts as c 
						inner join contacts_rel_users as cr on cr.contact_id=c.contact_id 
						left join users as u on cr.user_id=u.user_id 
						where c.is_referral=1 and cr.user_id=".SQL_CLEAN($_SESSION['user_id']));
}


while ($data = SQL_ASSOC_ARRAY($sth)) {
	$filters[] = $data;
}

// die(var_dump($filters));
$smarty->assign('referrals_completed', $referrals_completed);
$smarty->assign('referrals_generated', $referrals_generated);
$smarty->assign('referrals_deactive', $referrals_deactive);
$smarty->assign('total_referrals', $total_referrals);
$smarty->assign('contact_referrals', $contact_referrals);
$smarty->assign('filters', $filters);
$smarty->display('referrals_report.tpl');
